/**
 *
 * Lowest Common SubString
 * For the DP solution the trick is we look the value at the left diagonal element and add 1 to it if the characters are equal
 *
 */


public class LowestCommonSubstring {

    public static String lcs(String first, String second){

        int lenFirst = first.length();
        int lenSecond = second.length();

        String result = new String();

        if(lenFirst == 0 || lenSecond == 0){
            return result;
        }

        int len = 0;
        int[][] cache = new int[lenFirst][lenSecond];
        for(int i =0 ;i < lenFirst; i++){
            for (int j =0; j < lenSecond; j++){
                if(first.charAt(i) == second.charAt(j)){
                    if(i==0 || j==0){
                        cache[i][j] = 1;
                    }
                    else{
                        cache[i][j] = cache[i-1][j-1] + 1;
                    }

                    if(len < cache[i][j]){
                        len = cache[i][j];
                        result = first.substring(i-len + 1, i+1);
                    }
                }
            }
        }

        return result;
    }


    public static void main(String[] args) {
        String first = new String("ABBA");
        String second = new String("ABA");

        System.out.println(lcs(first, second));

    }
}
