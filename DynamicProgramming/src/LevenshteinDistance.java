import java.util.Arrays;
import java.util.Set;

/**
 * Given two strings str1 and str2 and below operations that can performed on str1.
 * Find minimum number of edits (operations) required to convert ‘str1’ into ‘str2’.
 * Insert
 * Remove
 * Replace
 * All of the above operations are of equal cost.
 */


public class LevenshteinDistance {

    /**
     * Utility function that returns the min of the three arguments
     * @param a
     * @param b
     * @param c
     * @return
     */
    private static int min(int a, int b, int c){
        return Math.min(Math.min(a,b),c);
    }

    /**
     * Utility function for the editDistanceRec, complexity- exponenetial
     * @param str1 First String
     * @param str2 Second String
     * @param len1 length, initially passed as len(str1)
     * @param len2 length, initially passed as len(str2)
     * @return min edits required
     */
    private static int editDistanceRecUtil(char[] str1, char[] str2, int len1, int len2){
        if(len1 == 0){
            return len2;
        }

        if(len2 == 0){
            return len1;
        }

        // if characters on both the pointers are equal
        if(str1[len1-1] == str2[len2-1]){
            return editDistanceRecUtil(str1,str2,len1-1,len2-1);
        }

        return 1 + min( editDistanceRecUtil(str1, str2, len1, len2-1), // Insert operation
                        editDistanceRecUtil(str1, str2, len1-1, len2), // Remove operation
                        editDistanceRecUtil(str1, str2, len1-1, len2-1)); // Delete operation
    }

    public static int editDistanceRec(String str1, String str2){
        return editDistanceRecUtil(str1.toCharArray(), str2.toCharArray(), str1.length(), str2.length());
    }



    private static int editDistanceDPUtil(char[] str1, char[] str2){

        int[][] cache = new int[str1.length+1][str2.length+1];

        //initialise the 2D Array
        for(int i=0; i < cache[0].length; i++){
            cache[0][i] = i;
        }

        for(int i=0; i < cache.length; i++){
            cache[i][0] = i;
        }

        for(int i=1; i <= str1.length; i++){
            for(int j=1; j <= str2.length; j++){
                if(str1[i-1] == str2[j-1]){
                    cache[i][j] = cache[i-1][j-1];
                } else {
                    cache[i][j] = 1+ min(cache[i-1][j],cache[i][j-1],cache[i-1][j-1]);
                }
            }
        }
        System.out.println(Arrays.deepToString(cache));

        return cache[str1.length][str2.length];
    }


    public static int editDistanceDP(String str1, String str2){
        return editDistanceDPUtil(str1.toCharArray(), str2.toCharArray());
    }

    public static void main(String[] args) {
        String str1 = "abcded";
        String str2 = "azced";
        System.out.println(editDistanceRec(str1,str2));
        System.out.println(editDistanceDP(str1,str2));

    }

}
