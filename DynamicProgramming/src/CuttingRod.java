/**
 * Given a rod of length n inches and an array of prices that contains prices of all pieces of size smaller than n.
 * Determine the maximum value obtainable by cutting up the rod and selling the pieces
 */
public class CuttingRod {


    static public int recursion(int[] values, int n){

        if(n<=0){
            return 0;
        }

        int max = Integer.MIN_VALUE;

        for(int i=0; i< n; i++){
            max = Math.max(max, values[i] + recursion(values, n-i-1));
        }

        return max;
    }


    static public int dp(int[] values, int n){

        int cache[] = new int[n+1];
       // cache[0] = 0;

        for(int i=1; i <= n; ++i){
            int max = Integer.MIN_VALUE;
            for(int j=0; j < i; ++j){
                    max = Math.max(max, values[j] + cache[i-j-1]);
            }
            cache[i] = max;
        }

        return cache[n];
    }

    public static void main(String[] args) {

        int[] values = new int[]{1, 5, 8, 9, 10, 17, 17, 20};
        int result = 0;
        result = recursion(values, values.length);
        System.out.println(result);

        result = dp(values,values.length);
        System.out.println(result);

    }

}
