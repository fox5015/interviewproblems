
/**
 */


public class OptimalGameStrategy {

    static int sumMax(int[] arr, int start, int end){

        if(start > end ){
            return 0;
        }

        if(start == end){
            return arr[start];
        }

        int a = arr[start] + Math.min(sumMax(arr,start+1,end-1),sumMax(arr,start+2,end));
        int b = arr[end] + Math.min(sumMax(arr,start+1,end-1),sumMax(arr,start,end-2));

        return  Math.max(a,b);
    }


    public static void main(String[] args) {

        int[] arr = new int[]{8,15,3,7};
        System.out.println(sumMax(arr,0,arr.length-1));
    }

}
