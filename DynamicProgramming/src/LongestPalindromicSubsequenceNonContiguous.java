/**
 *  Given a string, find a longest palindromic subsequence but not necessarily contiguous in it.
 *
 */

public class LongestPalindromicSubsequenceNonContiguous {


    /**
     * helper function to calculate the longest length
     * @param ip character array of the input String
     * @param i left pointer
     * @param j right pointer
     * @return max length of the longest palindromic substring
     */
    private static int findPalindromeRecUtil(char[] ip, int i, int j){
        if(i == j){
            return 1;
        }

        //If there are two characters and they are same
        if(ip[i] == ip[j] && i+1 == j){
            return 2;
        }

        // If the first and last characters are same
        if(ip[i] == ip[j]){
            return 2 + findPalindromeRecUtil(ip, i+1, j-1);
        }

        else{
            return Math.max(findPalindromeRecUtil(ip, i+1, j),  findPalindromeRecUtil(ip, i, j-1));
        }
    }

    /**
     * Recursive function that takes O(2^n) time
     * @param input
     * @return length of the longest palindrome
     */
    public static int findPalindromeRec(String input){

        if(input == null || input.length() == 0){
            return 0;
        } else {
            return findPalindromeRecUtil(input.toCharArray(),0,input.length()-1);
        }
    }



    /**
     * Utility function to print the content of the dp
     * @param dp input cache used for storing data
     *
     */
    private void printMatrix(int [][] dp){
        for(int i=0;i<dp.length;i++){
            for(int j=0;j<dp.length;j++){
                System.out.print("  " + dp[i][j]);
            }
            System.out.println("");
        }
    }

    /**
     *
     * @param input input string whose longest (not necessarily contiguous has to be found)
     * @return last element in the first row where the data longest will propogate
     */
    public static int findPalindrome(String input){

        char[] ip = input.toCharArray();
        int[][] cache = new int[ip.length][ip.length];


        // make diagonal elements same as they are the palindrome of length 1
        for(int i=0; i<cache.length; i++){
            cache[i][i] = 1;
        }

        for(int sublength=2; sublength<=ip.length; sublength++){

            //start at i
            for(int i=0; i<=cache.length-sublength; i++){
                //end at j
                int j = i+sublength-1;

                //if only 2 characters and they are the same then set dp[i][j] = 2
                if(sublength == 2 && ip[i] == ip[j]){
                    cache[i][j] = 2;
                } else if (ip[i] == ip[j]){
                    /*
                    if greater than length 2 and first and last characters match,
                    add 2 to the calculated value of the center stripped of both ends
                    */
                    cache[i][j] = 2 + cache[i+1][j-1];
                } else {
                    //if not match, then take max of 2 possibilities
                    cache[i][j] = Math.max(cache[i+1][j], cache[i][j-1]);
                }
            }
        }

        return cache[0][cache.length-1];
    }


    public static void main(String arg[]){
        String strA = "AABCDEBAZ";
        System.out.println("Length of Longest Palindrome in '" + strA + "' is- " + findPalindrome(strA));
    }
}
