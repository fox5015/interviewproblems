/**
 * Best Time to Buy and Sell Stock III
 * Design an algorithm to find the maximum profit. You may complete at most two transactions.
 *
 * We use left[i] to track the maximum profit for transactions before i, and use right[i] to track the maximum profit
 * for transactions after i.
 *
 * Prices: 1 4 5 7 6 3 2 9
 * left = [0, 3, 4, 6, 6, 6, 6, 8]
 * right= [8, 7, 7, 7, 7, 7, 7, 0]
 *
 * we compute the forward max profit and save it.  Forward max profit means for each day i, we want to know the
 * max profit we can make no later than this day. Note that we only need to consider 1 transaction.
 *
 * If we are required exact 2 transactions, we must finish one transaction at some day i, and do the 2nd transaction
 * after that. The max profit is the sum of max profit befo
 *
 * re day i and after day i. Day i might be every day in the
 * price list, so there is a loop in the code.
 * Similar to the step(1), but in a reverse order, from the last day -1 to first, we already have the max profit
 * before day i, mp[i], and we can compute the max profit every time for i to n-1 days (n is the number of days),
 * similar to step(1).
 *
 * Time complexity: O(n)
 */

public class BestSellStock3 {

    public static int maxProfit(int[] prices){
        if (prices.length == 0 || prices == null){
            return 0;
        }

        int[] left = new int[prices.length];
        int[] right = new int[prices.length];

        int min = prices[0];
        for (int i = 1; i < prices.length; i++){
            min = Math.min(min, prices[i]);
            left[i] = Math.max(left[i-1], prices[i] - min);
        }

        int max = prices[prices.length - 1];
        right[right.length - 1] = 0;

        for (int i = prices.length-2; i > 0; i--){
            max = Math.max(max, prices[i]);
            right[i] = Math.max(right[i+1], max - prices[i]);
        }

        int profit = 0;
        for (int i = 0; i < prices.length; i++){
            profit = Math.max(profit, left[i] + right[i]);
        }
        return profit;
    }

    public static void main(String[] args) {
        int result = maxProfit( new int[]{1,4,5,7,6,3,2,9});
        System.out.println(result);
    }
}
