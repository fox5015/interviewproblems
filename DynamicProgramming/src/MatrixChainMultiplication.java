/**
 */


public class MatrixChainMultiplication {

    static int recursion(int[] arr, int start, int end){

        if(start == end){
            return 0;
        }

        int min = Integer.MAX_VALUE;
        int sum = 0;
        for(int k = start; k < end; k++){
            sum = recursion(arr, start, k) + recursion(arr, k+1, end) + arr[start-1]*arr[k]*arr[end];
            if(sum < min){
                min = sum;
            }
        }

        return min;
    }


    static int memoizationUtil(int[] arr, int start, int end, int [][]cache){
        if(start == end){
            return 0;
        }

        int sum =0;
        int min = Integer.MAX_VALUE;
        if(cache[start][end] == 0){
            for(int k= start; k< end; k++){
                if(cache[start][k] == 0){
                    cache[start][k] = memoizationUtil(arr, start, k, cache);
                }
                if(cache[k][end] == 0){
                    cache[k][end] = memoizationUtil(arr, k, end, cache);
                }
                sum = cache[start][k] + cache[k][end] + arr[start]*arr[k]*arr[end];
                if (sum < min)
                    min = sum;

            }
            cache[start][end] = min;
        }

        return cache[start][end];
    }

    static int memoization(int[] arr, int start, int end){
        int[][] cache = new int[arr.length][arr.length];
        return memoizationUtil(arr, 1, arr.length - 1, cache);
    }



    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5};
        int result = recursion(arr, 1, arr.length-1);
        int result2 = recursion(arr, 1, arr.length-1);
        System.out.println(result + "- " + result2);
    }
}
