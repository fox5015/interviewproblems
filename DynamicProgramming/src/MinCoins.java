/**
 * Coin change Problem
 */

public class MinCoins {


    public static int coinChange(int total, int[] coins){

        if(total == 0){
            return 0;
        }

        int min = total;
        for(int coin:coins){
            if(total - coin >= 0){
                int no = coinChange(total-coin, coins);
                if(min > no + 1){
                    min = no + 1;
                }
            }
        }
        return min;
    }

    public static int coinChangeDP(int total, int[] coins){

        int[] cache = new int[total];
        for(int i = 1;i < total; i++){
            cache[i] = -1;
        }
        return helper(total, coins, cache);
    }

    private static int helper(int total, int[] coins, int[] cache){

        if(total == 0){
            return 0;
        }

        int min = total;
        for(int coin:coins){
            if(total - coin >=0 ){
                int no;
                if(cache[total-coin] >= 0){
                    no = cache[total-coin];
                }
                else{
                    no = helper(total-coin, coins, cache);
                }

                if(min > no+1){
                    min = no + 1;
                }
            }
        }

        return min;
    }



    public static void main(String[] args) {
        System.out.println(coinChangeDP(32, new int[]{1,5,10,25}));

    }
}
