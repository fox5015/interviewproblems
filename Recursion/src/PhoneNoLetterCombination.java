import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Given a digit string, return all possible letter combinations that the number could represent.
 * (Check out your cellphone to see the mappings)
 * Input:Digit string "23", Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 */


public class PhoneNoLetterCombination {


    public static List<String> letterCombination(String digits){
        HashMap<Character, char[]> map = new HashMap<>();
        List<String> result = new ArrayList<>();
        if (digits == null){
            return result;
        }
        map.put('2', new char[]{'a','b','c'});
        map.put('3', new char[]{'d','e','f'});
        map.put('4', new char[]{'g','h','i'});
        map.put('5', new char[]{'j','k','l'});
        map.put('6', new char[]{'m','n','o'});
        map.put('7', new char[]{'p','q','r','s'});
        map.put('8', new char[]{'t','u','v'});
        map.put('9', new char[]{'w','x','y','z'});

        helper(result, digits, new StringBuilder(), 0, map);
        return result;
    }

    private static void helper(List<String> result, String digits, StringBuilder sb, int index, HashMap<Character, char[]> map){

        if (index >= digits.length()){
            result.add(sb.toString());
            return;
        }

        char ch = digits.charAt(index);
        char[] arr = map.get(ch);
        for (int i = 0; i < arr.length; i++){
            sb.append(arr[i]);
            helper(result,digits,sb,index+1,map);
            sb.deleteCharAt(sb.length()-1);
        }

    }



    public static void main(String[] args) {
        List<String> result = new ArrayList<>();
        result = letterCombination("237");
        for (int i=0; i < result.size(); i++){
            System.out.print(result.get(i) + " ");
        }
    }

}
