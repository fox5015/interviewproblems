import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Generate Permutations
 */

public class Permutations {

    private static void util(int index, List<Integer> arr, List<List<Integer>> result){
        if(index == arr.size() - 1){
            result.add(new ArrayList<>(arr));
            return;
        }

        for(int i = index; i < arr.size(); i++){
            Collections.swap(arr,index,i);
            util(index + 1, arr, result);
            Collections.swap(arr,i,index);
        }
    }

    public static List<List<Integer>> permutation(List<Integer> arr){
        List<List<Integer>> result = new ArrayList<>();
        util(0, arr, result);
        return result;
    }

    public static void main(String[] args) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> input = new ArrayList<>();

        for(int i =0; i < 4; i++){
            input.add(i);
        }

        result = permutation(input);

        for (List<Integer> list : result) {

            for (Integer integer : list) {
                String strInteger = String.valueOf(integer);
                System.out.print(strInteger+ " ");
            }
            System.out.println();
        }
    }

}
