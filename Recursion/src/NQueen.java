import java.util.ArrayList;
import java.util.List;

/**
 * N Queen problem using backtracking
 */
public class NQueen {

    private static boolean isValid(List<Integer> colPlacement){
        int row = colPlacement.size()-1;
        for(int i = 0; i < row ; i++) {
            int diff = Math.abs(colPlacement.get(i) - colPlacement.get(row));
            if (diff == 0 || diff == row - i){
                return false;
            }
        }
        return true;
    }

    private static void solveNQueens(int n, int row, List<Integer> colPlacement, List<List<Integer>> result){
        if(row == n){
            result.add(new ArrayList<Integer>(colPlacement));
        }
        else{
            for(int col = 0; col < n; col++){
                colPlacement.add(col);
                if(isValid(colPlacement)){
                    solveNQueens(n,row+1,colPlacement,result);
                }
                colPlacement.remove(colPlacement.size()-1);
            }
        }

    }

    public static List<List<Integer>> nQueens(int n){
        List<List<Integer>> result = new ArrayList<>();
        solveNQueens(n, 0, new ArrayList<Integer>(), result);
        return result;
    }

    public static void main(String[] args) {
        List<List<Integer>> result = new ArrayList<>();
        result = nQueens(4);

        for (List<Integer> list : result) {

            for (Integer integer : list) {
                String strInteger = String.valueOf(integer);
                System.out.print(strInteger+ " ");
            }
            System.out.println();
        }
    }


}
