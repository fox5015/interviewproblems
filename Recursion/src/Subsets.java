/**
 * Print all subsets in a set
 */


public class Subsets {

    //Top down approach
    private static void printSubsets(int[] set, int setIndex, int[] subset, int subsetIndex){
        if(setIndex == set.length){
            System.out.print("{");
            for(int i=0;i<subsetIndex;i++){
                System.out.print(subset[i]);
                if(i+1 != subsetIndex){
                    System.out.print(",");
                }
            }
            System.out.println("}");
            return;
        }

        //Dont include in the subset
        printSubsets(set,setIndex+1,subset,subsetIndex);
        subset[subsetIndex] = set[setIndex];
        printSubsets(set,setIndex+1,subset,subsetIndex+1);

    }

    public static void printSubsets(int[] set) {
        int[] subset = new int[set.length];
        printSubsets(set, 0, subset, 0);
    }

    public static void main(String[] args) {
        int[] set = new int[]{1,2,3};
        printSubsets(set);

    }
}
