import java.util.*;

/**
 * Write a program that takes as input a set of words and returns group of anagrams of those words
 * EPI hashtables chapter problem 1
 */

public class GroupOfAnagrams {


    public static List<List<String>> findAnagrams(List<String> dictionary){

        /*
            create a map that will have sorted String as key and the corresponding strings values
         */
        Map<String, List<String>> sortedAnagrams = new HashMap<>();

        for (String s:dictionary){

            char[] sortedChars = s.toCharArray();
            Arrays.sort(sortedChars);
            String sortedStr = new String(sortedChars);

            //if the key is not present in the map create it
            if(!sortedAnagrams.containsKey(sortedStr)){
                sortedAnagrams.put(sortedStr, new ArrayList<String>());
            }
            sortedAnagrams.get(sortedStr).add(s);
        }


        List<List<String>> anagramGroups = new ArrayList<>();
        for (Map.Entry<String, List<String>> map: sortedAnagrams.entrySet()){
            if(map.getValue().size() >= 2){
                anagramGroups.add(map.getValue());
            }
        }

        return anagramGroups;
    }

    public static void main(String[] args) {
        List<String> dictionary = Arrays.asList("debit card", "bad credit", "the morse code",
                "here come dots", "the eyes", "they see", "THL");

        List<List<String>> result = findAnagrams(dictionary);
        System.out.println(Arrays.toString(result.toArray()));

    }



}
