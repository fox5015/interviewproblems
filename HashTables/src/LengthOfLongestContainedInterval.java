import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Find the length of a longest contained interval
 * Write a program which takes as input a set of integers represented by an array, and returns the size of a largest
 * subset of integers in the array having the property that if two integers are in subset, then so are all integers
 * between them
 * ed. if input is {3,-2,7,9,8,1,2,0,-1,5,8}
 * then the largest subset is {-2,-1,0,1,2,3}, so you should return 6
 *
 * Solution: Use upper and lower bound of particular number
 */

public class LengthOfLongestContainedInterval {

    public static int longestContainedRange(List<Integer> arr){

        Set<Integer> unprocessedEntries = new HashSet<>(arr);

        int maxIntervalSize = 0;

        while (!unprocessedEntries.isEmpty()){
            int a = unprocessedEntries.iterator().next();
            unprocessedEntries.remove(a);

            //Find lower bound
            int lowerBound = a - 1;
            while (unprocessedEntries.contains(lowerBound)){
                unprocessedEntries.remove(lowerBound);
                lowerBound--;
            }

            //Find upper Bound
            int upperBound = a + 1;
            while (unprocessedEntries.contains(upperBound)){
                unprocessedEntries.remove(upperBound);
                upperBound++;
            }

            maxIntervalSize = Math.max(upperBound - lowerBound - 1, maxIntervalSize);
        }

        return maxIntervalSize;
    }

    public static void main(String[] args) {

        List<Integer> input = Arrays.asList(3,-2,7,9,8,1,2,0,-1,5,8);
        System.out.println(longestContainedRange(input));
    }
}
