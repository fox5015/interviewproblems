import java.util.HashMap;
import java.util.Map;

/**
 * WAP that takes text for an anonymous letter and text for a magzine and determines if it is possible to write
 * anonymous letter letter using magazine
 * EPI HashMap (2) problem
 */

public class AnonymousLetter {

    public static boolean isLetterConstructibleFromMagzine(String letter, String magazine){
        Map<Character, Integer> charFrequency = new HashMap<>();

        for(int i = 0; i < letter.length(); i++){
            char ch = letter.charAt(i);
            if(!charFrequency.containsKey(ch)){
                charFrequency.put(ch, 1);
            } else {
                charFrequency.put(ch, charFrequency.get(ch) + 1);
            }
        }

        for(char ch: magazine.toCharArray()){
            if(charFrequency.containsKey(ch)){
                charFrequency.put(ch, charFrequency.get(ch) - 1);
                if(charFrequency.get(ch) == 0){
                    charFrequency.remove(ch);
                    if(charFrequency.isEmpty()){
                        break;
                    }
                }
            }
        }

        return charFrequency.isEmpty();
    }


    public static void main(String[] args) {

        System.out.println(isLetterConstructibleFromMagzine("abc", "abcd"));
    }
}
