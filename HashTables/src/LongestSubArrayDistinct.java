import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * WAP to find longest subArray with distinct elements
 * Input: f,s,f,e,t,w,e,n,w,e
 * Output: 5 (s,f,e,t,w)
 */


public class LongestSubArrayDistinct {

    public static int longestSubArrayWithDistinctElements(List<Integer> arr){

        // Key as the integer and last recent position as the value
        Map<Integer, Integer> mostRecentOcc = new HashMap<>();
        int longestDistinctStartIndexSoFar = 0;
        int result = 0;

        for (int i = 0; i < arr.size(); i++){

            // HashMap returns the previous value associate with key if present
            Integer dupIndex = mostRecentOcc.put(arr.get(i),i);

            if(dupIndex != null){
                if(dupIndex >= longestDistinctStartIndexSoFar){
                    result = Math.max(i - longestDistinctStartIndexSoFar, result);
                    longestDistinctStartIndexSoFar = dupIndex + 1;
                }
            }
        }

        return Math.max(result, arr.size() - longestDistinctStartIndexSoFar);
    }


    public static void main(String[] args) {
        System.out.println(longestSubArrayWithDistinctElements(Arrays.asList(1,2,3,3,3,8,4,10)));
    }
}
