/**
 * Given a linked list, remove the nth node from the end of list and return its head.
 * Given linked list: 1->2->3->4->5, and n = 2.
 * After removing the second node from the end, the linked list becomes 1->2->3->5.
 * Solution: use two pointer strategy
 */

public class DeleteKthNodeFromLast {

    public static ListNode<Integer> deleteKthNodeFromLast(ListNode<Integer> L, int k){

        if (L == null || L.next == null){
            return null;
        }

        ListNode<Integer> slow = L;
        ListNode<Integer> fast = L;

        for (int i = 0; i < k; i++){
            fast = fast.next;
        }

        while (fast != null && fast.next != null){
            fast = fast.next;
            slow = slow.next;
        }

        if (fast != null){
            slow.next = slow.next.next;
        } else {
            L = L.next;
        }

        return L;
    }


    public static void main(String[] args) {
        ListNode<Integer> L;
        L = new ListNode<>(
                1,
                new ListNode<>(
                        2, new ListNode<>(3, new ListNode<>(4, new ListNode<>(5, null)))));
        System.out.println(deleteKthNodeFromLast(L, 2).traverseLinkedList());
    }
}
