/**
 *
 * Merge sort a LinkedList
 * As Singly Linked list can be accessed in only one direction and cannot be accessed randomly, Quick sort will not be well suitable here.
 * Starting from the end and moving backward is usually expensive operation in Singly linked list.
 *  So Quick sort is well suited for arrays and not linked list. 
 *  Merge sort is a divide and conquer algorithm in which need of Random access to elements is less.
 *  So Merge Sort can be used for sorting Linked list.
 */


public class MergeSortLL {

    public static class LinkedListNode{
        int val;
        LinkedListNode next;

        LinkedListNode(int node_value) {
            val = node_value;
            next = null;
        }
    };

    static LinkedListNode getMiddle(LinkedListNode startNode){

        if(startNode == null){
            return startNode;
        }
        LinkedListNode slow = startNode;
        LinkedListNode fast = startNode;
        while(fast!=null && fast.next!=null && fast.next.next!=null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    static LinkedListNode mergeSortList(LinkedListNode pList) {

        if(pList == null || pList.next == null){
            return pList;
        }

        LinkedListNode middle = getMiddle(pList);
        LinkedListNode nextOfMiddle = middle.next;
        middle.next = null;

        LinkedListNode left = mergeSortList(pList);
        LinkedListNode right = mergeSortList(nextOfMiddle);

        LinkedListNode sorted = merge(left,right);
        return sorted;
    }

    static LinkedListNode merge(LinkedListNode left, LinkedListNode right){

        if(left == null){
            return right;
        }
        if(right == null){
            return left;
        }

        LinkedListNode temp = new LinkedListNode(0);
        if(left.val < right.val){
            temp.val = left.val;
            temp.next = merge(left.next,right);
        }

        if(left.val > right.val){
            temp.val = right.val;
            temp.next = merge(left, right.next);
        }

        return temp;
    }

    public static void printLinkList(LinkedListNode startNode) {
        LinkedListNode temp = startNode;
        while(temp!=null){
            System.out.print(temp.val + " ");
            temp = temp.next;
        }
    }

    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(10);
        LinkedListNode node2 = new LinkedListNode(1);
        LinkedListNode node3 = new LinkedListNode(-2);
        LinkedListNode node4 = new LinkedListNode(8);
        LinkedListNode node5 = new LinkedListNode(9);
        LinkedListNode node6 = new LinkedListNode(10);
        LinkedListNode node7 = new LinkedListNode(1);

        node1.next = node2 ;
        node2.next = node3;
        node3.next = (node4);
        node4.next = (node5);
        node5.next = (node6);
        node6.next = (node7);

        LinkedListNode startNode = node1;

        LinkedListNode sortedStartNode = mergeSortList(startNode);
        printLinkList(sortedStartNode);
    }


}
