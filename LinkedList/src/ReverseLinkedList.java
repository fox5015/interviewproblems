/**
 * Reverse LinkedList
 */
public class ReverseLinkedList {

    public ListNode<Integer> reverse(ListNode<Integer> l){
        ListNode<Integer> current = l;
        ListNode<Integer> next, prev = null;
        while (current != null){
            next= current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        l = prev;
        return l;
    }

    public static void main(String[] args) {
        ReverseLinkedList obj = new ReverseLinkedList();
        ListNode<Integer> L= new ListNode<>(3, new ListNode<>(1, new ListNode<>(4, null)));
        System.out.println(obj.reverse(L).traverseLinkedList());
    }
}
