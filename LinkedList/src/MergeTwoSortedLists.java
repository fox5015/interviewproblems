/**
 *  Merge two sorted Linked lists
 *  2-> 4 -> 7
 *  3 - > 11
 *  Result = 2- > 3 -> 4 -> 7 -> 11
 */
public class MergeTwoSortedLists {


    public static ListNode<Integer> mergeTwoSortedLinkedList(ListNode<Integer> l1, ListNode<Integer> l2){

        ListNode<Integer> dummyHead = new ListNode<>(0, null);
        ListNode<Integer> iter = dummyHead;
        ListNode<Integer> p1 = l1, p2 = l2;

        while (p1 != null && p2 != null){
            if (p1.data <= p2.data){
                iter.next = p1;
                p1 = p1.next;
            } else{
                iter.next = p2;
                p2 = p2.next;
            }
            iter = iter.next;
        }
        iter.next = p1 != null ? p1 : p2;
        return dummyHead.next;
    }

    public static void main(String[] args) {
        ListNode<Integer> L= new ListNode<>(1, new ListNode<>(2, new ListNode<>(3, null)));
        ListNode<Integer> R = new ListNode<>(3, new ListNode<>( 4, new ListNode<>(6, null)));
        ListNode<Integer> result = mergeTwoSortedLinkedList(L ,R);
        System.out.println(result.traverseLinkedList());

    }

}
