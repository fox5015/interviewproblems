/**
 *
 * Clone LinkedList which contains next and random pointer
 * http://www.geeksforgeeks.org/a-linked-list-with-next-and-arbit-pointer/
 *
 *
 */
public class CloneLinkedList {

     static class Node{
        int value;
        Node next;
        Node random;

         Node(int x){
             this.value = x;
         }

    }

    public static void print(Node head) {
        Node temp = head;
        while (temp != null) {
            Node random = temp.random;
            int randomData = (random != null) ? random.value : -1;
            System.out.println("Data = " + temp.value +
                    ", Random data = " + randomData);
            temp = temp.next;
        }
    }


    public static Node cloneNode(Node n){

        if(n == null){
            return n;
        }

        Node current = n;

        while(current!=null){
            Node temp = new Node(current.value);
            temp.next = current.next;
            current.next = temp;
            current = current.next.next;
        }

        current = n;

        while (current!=null){
            if(current.next.random != null)
                current.next.random = current.random.next;
            current = current.next.next;
        }

        Node copy = n.next;
        current = n;

        while(current.next != null){
            Node temp = current.next;
            current.next = current.next.next;
            current = temp;
        }

        return copy;
    }

    public static void main(String[] args) {

        Node randomListNode = new Node(-1);
        Node randomListNode1 = new Node(4);
        Node randomListNode2 = new Node(8);
        Node randomListNode3 = new Node(-3);
        Node randomListNode4 = new Node(7);
        randomListNode.next = randomListNode1;
        randomListNode1.next = randomListNode2;
        randomListNode2.next = randomListNode3;
        randomListNode3.next = randomListNode4;
        randomListNode.random = randomListNode1;
        randomListNode2.random = randomListNode3;
        randomListNode1.random = randomListNode;


        Node copy = cloneNode(randomListNode);
        print(copy);

    }


}
