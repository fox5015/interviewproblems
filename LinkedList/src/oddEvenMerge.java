/**
 *
 * Given a singly linked list, group all odd nodes together followed by the even nodes.
 *
 * Example:
 * Given 1->2->3->4->5->NULL,
 * return 1->3->5->2->4->NULL.
 *
 * The problem has two cases to consider: the length of the list is even and odd, respectively.
 We use two pointers, p1 and p2, to trace the odd and even nodes, respectively.
 If the length of the list is even, we stop at p2.next == null, i.e, when p2 reaches to the last node.
 In this case, p1 is in the node next to the last. e.g.
 1 -> 2 -> 3 -> 4 -> null
           |      |
            p1    p2

 If the length of the list is odd, we stop at p2 == null, i.e, p1 is in the last node.
 1 -> 2 -> 3 -> 4 -> 5 -> null
                     |      |
                      p1   p2

 We can observe that p1 is always one step below the p2, and we must let p1 stop at the last node of the odd list,
 then finally p1.next = evenHead. That is, we cannot lose the tail of the odd list, i.e., let p1 = null.
 */

public class oddEvenMerge {

    public static ListNode<Integer> formOddEvenMerge(ListNode<Integer> head){

        if (head == null || head.next == null){
            return head;
        }

        ListNode<Integer> odd = head;
        ListNode<Integer> even = head.next;
        ListNode<Integer> evenHead = even;

        while (even != null && even.next != null){
            odd.next = even.next;
            odd = odd.next;

            even.next = odd.next;
            even = even.next;
        }

        odd.next = evenHead;
        return head;
    }

    public static void main(String[] args) {

        ListNode<Integer> L= new ListNode<>(1, new ListNode<>(2, new ListNode<>(3,
                new ListNode<>(4, new ListNode<>(5, null)))));

        ListNode result = formOddEvenMerge(L);
        System.out.println(result.traverseLinkedList());
    }

}
