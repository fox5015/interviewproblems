/**
 * Remove duplicates from sorted linkedLists
 */
public class RemoveDuplicatesFromLL {

    public static ListNode<Integer> removeDuplicates(ListNode<Integer> L){
        ListNode<Integer> iter = L;

        while (iter != null){
            ListNode<Integer> nextDistinct = iter.next;
            while (nextDistinct != null && nextDistinct.data == iter.data){
                nextDistinct = nextDistinct.next;
            }
            iter.next = nextDistinct;
            iter = nextDistinct;
        }

        return L;
    }


    public static void main(String[] args) {

        ListNode<Integer> L= new ListNode<>(1, new ListNode<>(2, new ListNode<>(2,
                new ListNode<>(4, new ListNode<>(5, null)))));

        System.out.println(removeDuplicates(L).traverseLinkedList());
    }
}
