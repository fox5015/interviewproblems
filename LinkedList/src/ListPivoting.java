
/**
 * Implement a function which takes input as the SLL and integer K and performs a pivot of the list with respect to k.
 * The relative ordering of nodes that appear before k and after k must remain unchanged, the same must hold for
 * nodes holding keys equal to k
 */

public class ListPivoting {

    public static ListNode<Integer> listPivoting (ListNode<Integer> l, int k){

        ListNode<Integer> lessHead = new ListNode<>(0, null);
        ListNode<Integer> equalHead= new ListNode<>(0, null);
        ListNode<Integer> greaterHead = new ListNode<>(0, null);

        ListNode<Integer> lessIter = lessHead;
        ListNode<Integer> equalIter = equalHead;
        ListNode<Integer> greaterIter = greaterHead;

        ListNode<Integer> iter = l;

        while (iter != null){
            if (iter.data < k){
                lessIter.next = iter;
                lessIter = iter;
            }

            if (iter.data > k){
                greaterIter.next = iter;
                greaterIter = iter;
            }

            if (iter.data == k){
                equalIter.next = iter;
                equalIter = iter;
            }

            iter = iter.next;
        }


        lessIter.next = equalIter;
        equalIter.next = greaterIter;
        greaterIter.next = null;

        return lessHead.next;

    }


    public static void main(String[] args) {
        ListNode<Integer> L;
        L = new ListNode<>(
                1,
                new ListNode<>(
                        4, new ListNode<>(3, new ListNode<>(2, new ListNode<>(5, null)))));
        int x = 4;
        ListNode<Integer> result = listPivoting(L, x);
        System.out.println(result.traverseLinkedList());
    }

}
