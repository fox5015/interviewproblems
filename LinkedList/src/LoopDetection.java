/**
 * Determine if there is loop in the linkedList and returns the node of the beginning of the loop
 * Solution: Move slow pointer by 1 and fast pointer by 2
 * If they meet then there is a loop. In order to detect the starting of the loop, move slow pointer to head and move
 * both pointers simultaneously to detect the starting of the loop
 */

public class LoopDetection {

    public static ListNode<Integer> findBeginning(ListNode<Integer> head){

        ListNode<Integer> slow = head;
        ListNode<Integer> fast = head;

        while (fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
            //check for collision
            if (slow == fast){
                break;
            }
        }

        //Error check no meeting point so no loop
        if (fast == null || fast.next == null){
            return null;
        }

        slow = head;
        while (slow != fast){
            slow = slow.next;
            fast = fast.next;
        }

        return fast;
    }

}
