/**
 * Add two numbers
 * L1: head -> 3 -> 1 -> 4
 * L2: head -> 7 -> 0 -> 9
 * so 413 + 907 = 1320
 */
public class AddTwoNumbers {

    public static ListNode<Integer> addTwoNumbers(ListNode<Integer> num1, ListNode<Integer> num2){

        ListNode<Integer> dummyHead = new ListNode<>(0, null);
        ListNode<Integer> resultIter = dummyHead;
        int carry = 0;
        while (num1 != null || num2 != null){
            int sum = carry;
            if (num1 != null){
                sum = sum + num1.data;
                num1 = num1.next;
            }
            if (num2 != null){
                sum = sum + num2.data;
                num2 = num2.next;
            }
            resultIter.next = new ListNode<>(sum % 10, null);
            carry = sum / 10;
            resultIter = resultIter.next;
        }

        if (carry > 0){
            resultIter.next = new ListNode<>(carry, null);
        }

        return dummyHead.next;
    }

    public static void main(String[] args) {
        ListNode<Integer> L= new ListNode<>(3, new ListNode<>(1, new ListNode<>(4, null)));
        ListNode<Integer> R = new ListNode<>(7, new ListNode<>(0, new ListNode<>(9, null)));

        ListNode result = addTwoNumbers(L, R);
        System.out.println(result.traverseLinkedList());
    }
}
