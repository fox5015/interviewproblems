/**
 * From EPI
 */
import java.util.ArrayList;
import java.util.List;

class ListNode<T> {
    public T data;
    public ListNode<T> next;

    ListNode(T data, ListNode<T> next) {
        this.data = data;
        this.next = next;
    }

    //traverse the linkedList
    public List<T> traverseLinkedList(){

        ListNode<T> temp = this;
        List<T> result = new ArrayList<>();
        while (temp != null){
            result.add(temp.data);
            temp = temp.next;
        }
        return result;
    }



}

