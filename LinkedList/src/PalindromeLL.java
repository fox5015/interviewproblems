/**
 * check if this LinkedList is palindrome
 * Reverse the second half and check if they are equal
 */

public class PalindromeLL {

    public static boolean isPalindrome(ListNode<Integer> L){
        if (L == null){
            return true;
        }

        ListNode<Integer> slow = L;
        ListNode<Integer> fast = L;
        while (fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }

        ListNode<Integer> firstHalf = L;
        ListNode<Integer> secondHalf = new ReverseLinkedList().reverse(slow);

        while (firstHalf != null && secondHalf != null){
            if (firstHalf.data == secondHalf.data){
                firstHalf = firstHalf.next;
                secondHalf = secondHalf.next;
            } else {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        ListNode<Integer> L;
        L = new ListNode<>(
                1,
                new ListNode<>(
                        4, new ListNode<>(3, new ListNode<>(4, new ListNode<>(1, null)))));

        System.out.println(isPalindrome(L));
    }
}
