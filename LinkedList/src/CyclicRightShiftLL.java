import java.util.List;

/**
 * Cyclic right shift the linkedlist by K units
 * Hint: make it circular link list
 */
public class CyclicRightShiftLL {

    public static ListNode<Integer> cyclicRightShift(ListNode<Integer> L, int k){
        if (L == null){
            return L;
        }

        int length = 1;
        ListNode<Integer> tail = L;
        while (tail.next != null){
            length++;
            tail = tail.next;
        }

        k = k % length;
        if (k == 0){
            return L;
        }
        //Make cycle
        tail.next = L;
        ListNode<Integer> newTail = tail;
        int stepsForward = length - k;

        while (stepsForward-- > 0){
            newTail = newTail.next;
        }

        ListNode<Integer> newHead = newTail.next;
        newTail.next = null;
        return newHead;
    }


    public static void main(String[] args) {

        //1 2 3 4 5
        ListNode<Integer> L = new ListNode<>(1,
                new ListNode<>(2, new ListNode<>(3,
                        new ListNode<>(4, new ListNode<>(5, null)))));
        System.out.println(cyclicRightShift(L , 3).traverseLinkedList());
    }
}
