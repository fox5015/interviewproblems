/**
 * Remove duplicates from non sorted LinkedList
 */

import java.util.HashSet;

public class RemoveDuplicatesFromNotSortedLL {

    public static ListNode<Integer> deleteDup(ListNode<Integer> head){
        if(head == null){
            return null;
        }

        ListNode<Integer> iter = head;
        HashSet<Integer> set = new HashSet<>();
        ListNode<Integer> prev = null;

        while(iter != null){
            if(set.contains(iter.data)){
                prev.next = iter.next;
            }
            else {
                set.add(iter.data);
                prev = iter;
            }
            iter = iter.next;
        }

        return head;
    }


    public static void main(String[] args) {
        ListNode<Integer> L= new ListNode<>(2, new ListNode<>(2, new ListNode<>(1,
                new ListNode<>(4, new ListNode<>(6, null)))));

        System.out.println(deleteDup(L).traverseLinkedList());

    }
}
