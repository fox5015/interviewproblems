import java.util.HashMap;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and set.
 * get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
 * set(key, value) - Set or insert the value if the key is not already present.
 * When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
 */

class Node{
    int key;
    int value;
    Node prev;
    Node next;

    public Node(int key, int value) {
        this.key = key;
        this.value = value;
    }
}

public class LRUCache {
    int capacity;
    HashMap<Integer,Node> map = new HashMap<Integer,Node>();
    Node head = null;
    Node end = null;

    public LRUCache(int capacity){
        this.capacity = capacity;
    }

    public int get(int key){

        if(map.containsKey(key)){
            Node n = map.get(key);
            remove(n);
            setHead(n);
            return n.value;
        }
        return -1;
    }

    private void remove(Node n){
        if(n.prev != null){
            n.prev.next = n.next;
        } else {
            head = n.next;
        }

        if(n.next != null){
            n.next.prev = n.prev;
        } else {
            end = n.prev;
        }
    }

    private void setHead(Node n){

        n.next = head;
        n.prev = null;

        if(head != null){
            head.prev = n;
        }

        head = n;

        if(end == null){
            end = head;
        }
    }

    public void set(int key, int value){

        if(map.containsKey(key)){
            Node old = map.get(key);
            old.value = value;
            remove(old);
            setHead(old);
        } else {
            Node created = new Node(key, value);
            if(map.size() > capacity){
                map.remove(end.key);
                remove(end);
                setHead(created);
            } else {
                setHead(created);
            }
            map.put(key,created);
        }
    }

    public void printKeyPriority(){

        Node temp = head;
        while (temp !=null){
            System.out.print("(" + temp.key + "," + temp.value + ")");
            temp = temp.next;
        }
        System.out.println("\n");
    }


    public static void main(String[] args) {
        LRUCache lr=new LRUCache(5);
        lr.set(1, 1);
        lr.set(2,2);
        lr.set(3, 3);
        lr.set(4, 4);
        lr.set(5, 5);
        int val=lr.get(1);
        System.out.println(""+val);
        lr.set(6, 6);
        int val2=lr.get(2);
        System.out.println(""+val2);
        lr.printKeyPriority();

    }

}
