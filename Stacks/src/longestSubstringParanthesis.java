/**
 *
 * Ref: http://n00tc0d3r.blogspot.com/2013/04/longest-valid-parentheses.html
 */


public class longestSubstringParanthesis {


    private static int longestValidParentheses(String s, int start, int end, int step, char c){

        int count = 0;
        int max =0;
        int len = 0;
        for(int i=start;i<end;i=i+step){
            if(s.charAt(i) == c){
                count++;
            }else{
                if(count>0){
                    count--;
                    len = len + 2;
                    if(count ==0){
                        max = Math.max(len,max);
                    }
                }else{
                    len = 0;
                }
            }
        }

        return max;
    }

    public static int longestValidParentheses(String s) {
        return Math.max(longestValidParentheses(s, 0, s.length(), 1, '('),
                longestValidParentheses(s, s.length()-1, -1, -1, ')'));
    }

    public static void main(String[] args) {
        System.out.println(longestValidParentheses("())()()(()"));
    }


}
