import java.util.Stack;

/**
 *
 * Implement stack such that you get min element in O(1) time.
 * solution:
 * If a new element is larger than the current minimum, we do not need to push it on to the min stack. When we perform
 * the pop operation, check if the popped element is the same as the current minimum.
 * If it is, pop it off the min stack too.
 */



public class MinStack {

    private Stack<Integer> minStack = new Stack<>();
    private Stack<Integer> stack = new Stack<>();

    public void push(int x){
        stack.push(x);
        if (minStack.isEmpty() || x <= minStack.peek()){
            minStack.push(x);
        }
    }

    public void pop(){
        if (!minStack.isEmpty() && stack.pop().equals(minStack.peek())){
            minStack.pop();
        }
    }

    public int top(){
        return stack.peek();
    }

    public int getMin(){
        return minStack.peek();
    }

    public static void main(String args[])
    {
        MinStack mStack = new MinStack();
        mStack.push(7);
        mStack.push(8);
        System.out.println(mStack.getMin());
        mStack.push(5);
        mStack.push(9);
        System.out.println(mStack.getMin());
        mStack.push(5);
        mStack.push(2);
        System.out.println(mStack.getMin());
        mStack.pop();
        mStack.pop();
        System.out.println(mStack.getMin());
    }


}
