import java.util.Stack;

/**
 * Find the largest rectangular area possible in a given histogram where the largest rectangle can be made of
 * a number of contiguous bars. For simplicity, assume that all bars have same width and the width is 1 unit
 */

public class AreaHistogram {

    public static int maxArea(int[] input){
        int area = 0;
        int maxArea = 0;
        int i = 0;

        Stack<Integer> stack = new Stack<>();

        for (i = 0; i < input.length;){

            //if the stack is empty then put the element or keep on pushing as long as we find greater element
            if (stack.isEmpty() || input[i] >= input[stack.peek()]){
                    stack.push(i++);
            } else {
                int top = stack.pop();
                if (stack.isEmpty()){
                    area = input[top] * i;
                } else {
                    area = input[top] * (i - stack.peek() - 1);
                }

                maxArea = Math.max(area, maxArea);
            }
        }


        while (!stack.isEmpty()){
            int top = stack.pop();
            if (stack.isEmpty()){
                area = input[top] * i;
            } else {
                area = input[top] * (i - stack.peek() - 1);
            }

            maxArea = Math.max(area, maxArea);
        }

        return maxArea;
    }


    public static void main(String[] args) {
        int hist[] = {6, 2, 5, 4, 5, 1, 6};
        System.out.println(maxArea(hist));
    }
}
