/**
 * Implementation of Robin Karp algorithm
 */


public class RobinKarp {

    private static int prime = 3;


    /**
     * Logic is to calculate the hash like
     * Hash(str) = alphabetIndex(i) * prime ^0 + alphabetIndex + alphabetIndex(i+1) * prime^1 + ...
     * @param str input string whose hash needs to calculate
     * @param end the end index
     * @return
     */
    private static long createHash(char[] str, int end){
        long hash = 0;

        for (int i = 0; i <= end; ++i){
            hash = (long) (hash + str[i] * Math.pow(prime,i));
        }

        return hash;
    }


    private static long reCalHash(char[] str, int oldIndex, int newIndex, long oldHash, int patternLength){

        long newHash = oldHash - str[oldIndex];
        newHash = newHash / prime;
        newHash = (long) (newHash + str[newIndex] * Math.pow(prime, patternLength-1));
        return newHash;
    }

    private static boolean checkEqual(char[] str1, int start1, int end1, char[] str2, int start2, int end2){

        while(start1 <= end1 && start2 <= end2){
            if(str1[start1] != str2[start2]){
                return false;
            }
            start1++;
            start2++;
        }
        return true;
    }

    public static int pattenSearch(char[] text, char[] pattern){
        int pLen = pattern.length;
        int tLen = text.length;

        long patternHash =createHash(pattern, pLen - 1);
        long textHash = createHash(text, pLen-1);

        for (int i = 1; i <= tLen - pLen + 1; ++i){
            if (patternHash == textHash &&
                    checkEqual(text, i-1, i + pLen -2, pattern, 0, pLen-1)){
                return i-1;
            }
            if (i < tLen-pLen+1){
                textHash = reCalHash(text, i-1, i+pLen-1, textHash,pLen);
            }
        }

        return -1;
    }


    public static void main(String[] args) {
        System.out.println(pattenSearch("LionKing".toCharArray(), "on".toCharArray()));
    }

}
