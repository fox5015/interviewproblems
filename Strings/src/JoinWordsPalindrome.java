import java.util.*;

/**
 * Join words to make a palindrome
 * Given a list of words, is there any pair of words, that can be joined (in any order) to form a palindrome?
 *  Consider a list {bat, tab, cat}. Then bat and tab can be joined together to form a palindrome.
 *
 *  Solution:
 *
 *  1. Put all the reversed order of the input string into a Map. The key is the reversed order of the string,
 *       and the value is the indices of the word
 *  2. For each word, get all its prefixes, If the prefix is in the map AND the rest of the string is a palindrome,
 *      then we can find a pair where the first half is the current word,
 *      and the second half is the reversed order of prefix.
 *  3. For each word, get all its postfixes. If the postfix is in the map AND the first half of the string is palindrome,
 *      then we can find a pair where the reversed order of the postfix is the first part,
 *      and the word is the second part of the pair.
 *
 * The reason why we need to track the position of each word is to avoid the situation like ["c"],
 * i.e. the word itself is a palindrome.
 * Then we may mistakely concatenate a "cc" as a palindrome. So we can concatenate two words IFF
 * 1. The key in the map is different from the current word
 * 2. If they are the same, they must have different indices.

 */


public class JoinWordsPalindrome {


    private static boolean isPalindrome(String s) {
        if (s == null || s.length() <= 1) {
            return true;
        }

        int lo = 0;
        int hi = s.length() - 1;

        while (lo < hi) {
            if (s.charAt(lo) != s.charAt(hi)) {
                return false;
            }

            lo++;
            hi--;
        }

        return true;
    }


    public static List<String> getPalindromePairs(String[] input){

        Set<String> result = new HashSet<>();
        if (input.length == 0 || input == null){
            return new ArrayList<>();
        }

        //populate the map with reverse of the list
        Map<String, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < input.length; i++){
            String str = input[i];
            String strRev = new StringBuilder(str).reverse().toString();
            if (map.containsKey(strRev)){
                List<Integer> indices = map.get(strRev);
                indices.add(i);
                map.put(strRev,indices);
            } else {
                List<Integer> indices = new ArrayList<>();
                indices.add(i);
                map.put(strRev, indices);
            }
        }


        //Iterate each word
        for (int i = 0; i < input.length; i++){
            String str = input[i];

            for (int j = 0; j < str.length(); j++){
                String prefix = str.substring(0,j);
                String postfix = str.substring(j);
                if (map.containsKey(prefix) && isPalindrome(postfix)){
                    if (map.get(prefix).size() > 1 || !map.get(prefix).equals(str)){
                        String palin = str + new StringBuilder(prefix).reverse().toString();
                        result.add(palin);
                    }
                }
            }


            for (int j = str.length() - 1; j >= 0; j--) {
                String postfix = str.substring(j);
                String prefix = str.substring(0, j);

                if (map.containsKey(postfix) && isPalindrome(prefix)) {
                    if (map.get(postfix).size() > 1 || !map.get(postfix).equals(str)) {
                        String palin = new StringBuilder(postfix).reverse().toString() + str;
                        result.add(palin);
                    }
                }
            }
        }

        return new ArrayList<String>(result);

    }

    public static void main(String[] args) {
        String[] input = new String[]{"abc", "cba", "c", "c"};

        List<String> result = getPalindromePairs(input);

        for (String s : result) {
            System.out.println(s);
        }
    }
}
