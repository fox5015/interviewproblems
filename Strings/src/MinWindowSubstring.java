import java.util.HashMap;

/**
 * Minimum Window Substring
 *
 * Given a string S and a string T, find the minimum window in S which will contain all the characters in T.
 * e.g.
 * S = "AYZABOBECODXBANC" T = "ABC"
 * Minimum window is "BANC"
 *
 * Logic: The idea to find out solution in linear time is first find the substring that will contain all the characters
 * from T. Then we reduce the window by incrementing the start pointer.
 * We increment the start pointer only if character is not there in HashMap or the map contains full set of target
 * characters.
 *
 *
 */
public class MinWindowSubstring {


    public static String minSubstring(String S, String T){

        String result = new String();
        if (S == null || T == null || S.length() == 0 || T.length() == 0){
            return result;
        }

        HashMap<Character,Integer> dict = new HashMap<>();
        HashMap<Character,Integer> hasFound = new HashMap<>();

        //Populate the dictionary with the character count while populating hasFound with just chars init. to zero
        for (int i = 0; i < T.length(); i++){
            hasFound.put(T.charAt(i), 0);
            if (dict.containsKey(T.charAt(i))){
                dict.put(T.charAt(i),dict.get(T.charAt(i) +1));
            } else {
                dict.put(T.charAt(i), 1);
            }
        }

        int start = 0;
        int count = 0;
        int minLength = S.length() + 1;

        for (int end = 0; end < S.length(); end++) {

            if (hasFound.containsKey(S.charAt(end))) {
                hasFound.put(S.charAt(end), hasFound.get(S.charAt(end)) + 1);

                if (hasFound.get(S.charAt(end)) <= dict.get(S.charAt(end))) {
                    count++;
                }
            }

            if (count == T.length()) {
                while (!dict.containsKey(S.charAt(start)) || hasFound.get(S.charAt(start)) > dict.get(S.charAt(start))){
                    if (hasFound.containsKey(S.charAt(start))) {
                        hasFound.put(S.charAt(start), hasFound.get(S.charAt(start)) - 1);
                    }
                    start++;
                }

                if (minLength > end - start + 1) {
                    minLength = end - start + 1;
                    result = S.substring(start, end + 1);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        String S = new String("ADOBECODEBANC");
        String T = new String("ABC");

        System.out.println(minSubstring(S, T));
    }
}
