import java.util.HashMap;

/**
 * Longest SubString with K unique characters
 * For example, Given s = “eceba” and k = 2,
 * T is "ece" which its length is 3.
 * Logic:To solve it in O(n) time, we use hashmap to keep count of the characters as we travers
 */

public class LongestSubstringWithKUnique {


    public static int lengthOfLongestSubstringKDistinct(String str, int k){

        if (k <= 0 || str.length() == 0 || str == null){
            return 0;
        }

        HashMap<Character,Integer> map = new HashMap<>();
        int maxLen = 0;
        int endPtr = 0;
        int startPtr = 0;

        for (startPtr = 0; startPtr < str.length(); startPtr++){
            char ch = str.charAt(startPtr);
            if (map.containsKey(ch)){
                int temp = map.get(ch);
                map.put(ch, temp+1);
            } else {
                map.put(ch, 1);
            }

            if (map.size() > k){
                maxLen = Math.max(maxLen, startPtr - endPtr);

                while (map.size() > k){
                    char end = str.charAt(endPtr);
                    int freq = map.get(end);
                    if (freq == 1){
                        map.remove(ch);
                    } else {
                        map.put(end, freq - 1);
                    }
                    endPtr++;
                }
            }
        }

        if (endPtr < str.length()){
            maxLen = Math.max( startPtr - endPtr, maxLen);
        }

        return maxLen;
    }


    public static void main(String[] args) {
        String str = new String("eceba");
        int k = 2;
        System.out.println(lengthOfLongestSubstringKDistinct(str, k));
    }
}
