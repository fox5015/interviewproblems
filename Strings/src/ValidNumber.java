/**
 Validate if a given string is numeric.
 Some examples:
 "0" => true
 " 0.1 " => true
 "abc" => false
 "1 a" => false
 "2e10" => true

 * There are the flows to solve this question:
 1. Skip the leading white spaces, if there is any.
 2. Skip the + or - sign, if there is any.
 3. Skip the first segment of numerical characters.
 4. Skip the '.' character if there is any
 5. Skip the second segment of numerical characters. e.g 12.14
 6. If we consider the exponent character, e and E, skip that one if there is any
 7. Skip the + or - sign
 8. Skip the numerical characters followed by the E or e.
 9. Remove trialing white spaces, if there is any
 10. Check if we have reached the end of the string. If yes, return true. Else return false.

 Eg on the above to program
 "   +/- 1.24e/E +/- 124  "

 */
public class ValidNumber {


    public static boolean isNumber(String str){
        int n = str.length();
        int i = 0;

        //skip until we find white spaces
        while (i < n && str.charAt(i) == ' '){
            i++;
        }

        // step 2: Skip + or - sign
        if (i < n && (str.charAt(i) == '-' || str.charAt(i) == '+')){
            i++;
        }

        // step 3: skip the first segment of numeric characters
        boolean isNumeric = false;
        while (i < n && Character.isDigit(str.charAt(i))){
            i++;
            isNumeric = true;
        }


        // step 4 and 5 skip the . character and the following numeric characters, if any
        if (i < n && str.charAt(i) == '.' ){
            i++;
            while (i < n && Character.isDigit(str.charAt(i))){
                i++;
                isNumeric = true;
            }
        }

        // step 6 and 7 and 8, exponent character and following numeric characters
        if (isNumeric && i < n && (str.charAt(i) == 'e' || str.charAt(i) == 'E')){
            i++;
            isNumeric = false;
            if (i < n && (str.charAt(i) == '-' || str.charAt(i) == '+')){
                i++;
            }
            while (i < n && Character.isDigit(str.charAt(i))){
                i++;
                isNumeric = true;
            }
        }

        // step 9, remove trailing spaces
        while (i < n && str.charAt(i) == ' '){
            i++;
        }

        return isNumeric && i == n;
    }

    public static void main(String[] args) {
        System.out.println(isNumber("2.45"));
    }

}
