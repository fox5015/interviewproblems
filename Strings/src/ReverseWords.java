/**
 * Reverse the words in the string
 */
public class ReverseWords {


    /**
     * Reverses the words
     * @param input input character array that needs to be reversed
     */
    public static void reverseWords(char[] input){
        reverse(input, 0, input.length);

        int start = 0;
        int end;
        while ((end = find(input, ' ', start)) != -1){
            reverse(input, start, end);
            start = end + 1;
        }

        reverse(input, start, input.length);
    }


    /**
     * This function reverses the array
     * @param subArray Array to be reversed
     * @param start startIndex
     * @param end End Index
     */

    private static void reverse(char[] subArray, int start, int end){

        if (start >= end){
            return;
        }

        int last = end - 1;
        for (int i = start; i <= start + (last - start) / 2; i++){
            char temp = subArray[i];
            subArray[i] = subArray[last - i + start];
            subArray[last - i + start] = temp;
        }
    }


    /**
     * This function find the character's index if present or else returns -1 if not found
     * @param arr input array
     * @param ch character to find
     * @param start start index
     * @return index of the element
     */
    private static int find(char[] arr, char ch, int start){
        for (int i = start; i < arr.length; i++){
            if (arr[i] == ch){
                return i;
            }
        }
        return -1;
    }


    public static void main(String[] args) {

        char[] input = "Tom is Cat".toCharArray();
        reverseWords(input);
        System.out.println(input);
    }

}
