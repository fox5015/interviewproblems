/**
 * Generate numeronym
 *
 * Complexity can be further reduced if it can be stored in some sort of List, just too lazy to implement that
 */


public class Numeronym {


    public static String slice_range(String s, int startIndex, int endIndex) {
        if (startIndex < 0) startIndex = s.length() + startIndex;
        if (endIndex < 0) endIndex = s.length() + endIndex;
        return s.substring(startIndex, endIndex);
    }

    public static void generateNumeronym(String str){
        int length = str.length();
        //starting of the first character
        int start = 1;

        //ending of the first character
        int end = 1;
        int count = 0;

        //maximum size of the number in between the word
        int max = length - start - end;

        for (int n = length - 2; n > 0; n--){
            for (int front = 0; front < length; front++){
                count = front + start + end + n;
                if (count == length){
                    System.out.println(slice_range(str,0, front + 1) + n + str.charAt(length-1));
                }

                else if (count < length){
                    System.out.println(slice_range(str,0, front + 1) + n +
                                        slice_range(str,count-1,length));
                }
            }
        }
    }


    public static void main(String[] args) {

        String str = new String("Career");
        generateNumeronym(str);
    }

}
