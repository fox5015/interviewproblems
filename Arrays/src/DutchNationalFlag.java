import java.util.Arrays;

/**
 *
 */
public class DutchNationalFlag {


    public static void sort(int a[], int n)
    {
        int zero = 0;
        int two = n-1;
        int temp = 0;
        int current = 0;

        while(current <= two) {
            switch(a[current]) {
                case 0:
                    temp = a[zero];
                    a[zero] = a[current];
                    a[current] = temp;
                    zero++;
                    current++;
                    break;
                case 1:
                    current++;
                    break;
                case 2:
                    temp = a[current];
                    a[current] = a[two];
                    a[two] = temp;
                    two--;

                    break;
            }
        }
    }


    public static void main(String[] args) {
        int arr[] = {0, 1, 0, 1, 2};
        sort(arr, arr.length);
        System.out.println("Array after seggregation ");
        System.out.println(Arrays.toString(arr));

    }
}
