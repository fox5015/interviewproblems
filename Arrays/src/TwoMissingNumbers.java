import java.util.Arrays;

/**
 *  Find the two numbers missing from a sequence of integers
 *  input: arrary of integers ( no duplicates )
 *  output: array of integers (two values)
 *  Strategy: use XOR logic
 *
 */
public class TwoMissingNumbers {


    public static int[] findTwoMissingNumbers(int[] arr){

        int size = arr.length + 2;
        long totalSum = size * (size + 1) / 2;
        long arrSum = 0;

        for(int i:arr){
            arrSum += i;
        }

        int pivot = (int) ((totalSum - arrSum) / 2);

        int totalLeftXOR = 0;
        int totalRightXOR = 0;
        int arrLeftXOR = 0;
        int arrRightXOR = 0;

        for(int i:arr){

            if(i <= pivot){
                arrLeftXOR ^= i;
            }
            else{
                arrRightXOR ^= i;
            }
        }

        for(int i = 1; i <= pivot; i++){
            totalLeftXOR ^= i;
        }

        for(int i = pivot + 1; i <= size; i++){
            totalRightXOR ^= i;
        }

        return new int[]{arrLeftXOR ^ totalLeftXOR, arrRightXOR ^ totalRightXOR};
    }


    public static void main(String[] args) {
        int[] result = new int[2];
        result = findTwoMissingNumbers(new int[]{4,2,3});
        System.out.println(Arrays.toString(result));

    }
}
