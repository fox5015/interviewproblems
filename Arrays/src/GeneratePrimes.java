import java.util.ArrayList;
import java.util.List;

/**
 * Generate prime numbers from 1 .. n
 * Sieve of Eratosthenes
 * https://youtu.be/n6Jk1yGsml8
 */

public class GeneratePrimes {

    public static List<Integer> sieveOfEratosthenes(int n){
        List<Integer> result = new ArrayList<>();
        if (n <= 0){
            return result;
        }

        boolean[] prime = new boolean[n];
        prime[0] = false;
        for (int c = 1; c < n ; c++){
            prime[c] = true;
        }

        for (int i = 2; i <= n; i++){
            if (prime[i-1] == true){
                result.add(i);
                for (int j = 2*i; j <= n; j += i){
                    prime[j-1] = false;
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        List<Integer> result = sieveOfEratosthenes(100);
        System.out.println(result);
    }
}
