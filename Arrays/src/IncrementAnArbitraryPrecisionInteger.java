import java.util.Arrays;
import java.util.List;

/**
 * WAP which takes as input an array of digits encoding number D and updates the array to represent the number D+1
 * if input is {1,2,9} then it should be updated as {1,3,0}
 */

public class IncrementAnArbitraryPrecisionInteger {

    public static List<Integer> addOne(List<Integer> arr){
        int n = arr.size() - 1;
        arr.set(n, arr.get(n) + 1);

        for (int i = n; n > 0 && arr.get(i) == 10; --i){
            arr.set(i, 0);
            arr.set(i-1, arr.get(i-1) + 1);
        }

        if (arr.get(0) == 10){
            arr.set(0,0);
            arr.set(0,1);
        }

        return arr;
    }


    public static void main(String[] args) {
        List<Integer> intList = Arrays.asList(1, 9, 3, 9);
        List<Integer> result = addOne(intList);

        for (int i = 0; i < result.size(); i++){
            System.out.print(result.get(i) + "-");
        }
    }
}
