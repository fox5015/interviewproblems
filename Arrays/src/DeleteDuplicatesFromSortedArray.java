import java.util.Arrays;

/**
 * Delete Duplicates from sorted array
 */

public class DeleteDuplicatesFromSortedArray {

    public static int deleteDuplicates(int[] arr){

        if (arr.length == 0 && arr == null){
            return 0;
        }

        int writeIndex = 1;
        for (int i = 1; i < arr.length; i++){
            if (arr[i] != arr[writeIndex-1]){
                arr[writeIndex] = arr[i];
                writeIndex++;
            }
        }

        System.out.println(Arrays.toString(arr));
        return writeIndex;

    }

    public static void main(String[] args) {
        System.out.println(deleteDuplicates(new int[]{2,3,5,5,7,11,11,11,13}));
    }
}
