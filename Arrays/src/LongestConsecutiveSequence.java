import java.util.HashSet;

/**
 * Longest consecutive sequence of numbers in an unsorted array.
 */

public class LongestConsecutiveSequence {

    public static int findLongest(int[] arr){

        HashSet<Integer> set = new HashSet<>();
        for(int i:arr){
            set.add(i);
        }

        int max = 0;
        int length = 0;


        for(int i:set){

            if(set.contains(i-1)){
                continue;
            }

            length = 0;
            while(set.contains(i++)){
                length++;
            }

            max = Math.max(length,max);
        }

        return max;
    }


    public static void main(String[] args) {

        int[] arr = {4, 2, 1, 6, 5};
        System.out.println(findLongest(arr));
    }
}
