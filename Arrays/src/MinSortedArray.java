/**
 *
 * Find min in sorted Array
 * Hint: Use the fact the its sorted and use divide and conquer approach
 *
 */


public class MinSortedArray {

    public static int FindIndexOfMin(int[] arr){

        int length = arr.length;
        int low  = 0 , high = length-1;

        while (low <= high){
            if(arr[low] <= arr[high]){
                return low;
            }
            int mid = (low + high) / 2;
            int next = (mid + 1) % length;
            int prev = (mid -1 + length) % length;

            /**
             * property of the smallest element
             * eg. 11 12 15 18 2 5 6 8
             */
            if(arr[mid] <= arr[prev] && arr[mid] <= arr[next]){
                return mid;
            }

            if(arr[mid] >= arr[high]){
                //first half is sorted
                low = mid + 1;
            }

            if(arr[mid] <= arr[high]){
                //second half is sorted
                high = mid - 1;
            }
        }

        return -1;
    }


    public static void main(String[] args) {

        int[] arr = new int[]{4, 5, 1, 2, 3};
        int result = FindIndexOfMin(arr);
        System.out.println(result);

    }

}
