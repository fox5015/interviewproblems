/**
 * Given an array of size n, find the majority element. The majority element is the element that
 * appears more than ⌊ n/2 ⌋ times. You may assume that the array is non-empty and the majority element always
 * exist in the array.
 *
 * Solution: Use Boyer Moore algorithm
 * Initialise count of candidate to 1
 * Iterate over the array elements, increment count for each candidate
 * if Count becomes zero then initialize the candidate to that specific element and repeat
 * At the end of iteration you will get an element who appears more than half
 *
 * Traverse the array again to confirm the majority element
 * eg. 2, 1, 2, 4, 7
 * if we apply the previous algorithm: candidate will be 7 and count will be 1 which is not true
 * so traverse the array and confirm if candidate is majority element
 */

public class MajorityElement {

    public static Integer getMajorityElement(int[] arr){

        if (arr == null || arr.length == 0){
            return null;
        }

        Integer candidate = null;
        int count = 0;
        for (int i = 0; i < arr.length; i++){
            if (count == 0){
                candidate = arr[i];
                count++;
                continue;
            } else {
                if (candidate == arr[i]){
                    count++;
                } else {
                    count--;
                }
            }
        }

        if (count == 0){
            return null;
        }

        count = 0;

        for (int i = 0; i < arr.length; i++){
            if (candidate == arr[i]){
                count++;
            }
        }

        return (count > arr.length/2) ? candidate : -1;
    }


    public static void main(String[] args) {
        //2, 6, 2, 2, 6, 2, 2, 8, 2, 1
        //4, 7, 4, 4, 7, 4, 4, 9, 4, 3
        Integer result = getMajorityElement(new int[]{2, 1, 2, 4, 7});
        System.out.println(result);
    }
}
