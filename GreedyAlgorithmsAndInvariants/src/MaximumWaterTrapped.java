import java.util.Arrays;
import java.util.List;

/**
 * Given n non-negative integers representing an elevation map, compute how much water
 * it is able to trap after raining.
 *
 * EPI problem
 */

public class MaximumWaterTrapped {

    public static int getMaxTrappedWater(List<Integer> heights){
        int left = 0;
        int right = heights.size() - 1;
        int width = 0;
        int maxWater = 0;
        while (left < right){
            width = right - left;
            maxWater = Math.max(maxWater, width * Math.min(heights.get(left), heights.get(right)));
            if (heights.get(left) == heights.get(right)){
                left++;
                right--;
            } else if (heights.get(left) < heights.get(right)){
                left++;
            } else if (heights.get(left) > heights.get(right)){
                right--;
            }
        }
        return maxWater;
    }


    public static void main(String[] args) {
        int result = getMaxTrappedWater(Arrays.asList(1,2,1,3,4,4,5,6,2,1,3,1,3,2,1,2,4,1));
        System.out.println(result);
    }
}
