/**
 *
 * There are n students in a class. Every student can have 0 or more friends.
 * If A is a friend of B and B is a friend of C then A and C are also friends.
 * So we define a friend circle as a group of students who are friends as given by above definition.
 * Given an nXn-matrix friends which consists of characters Y or N.
 * If friends[i][j]=Y, then ith and jth students are friends, friends[i][j]=N, then i and j are not friends.
 * Find the total number of such friend circles in the class.
 */

public class FriendCirclesGraph {

    public static int findFriendsCircle(char[][] friends){

        if(friends == null || friends.length==0){
            return 0;
        }

        boolean[] visited = new boolean[friends.length];

        for(int i=0; i < visited.length; i++){
            visited[i] = false;
        }

        int count = 0;
        for(int i=0; i< visited.length; i++){
            if(!visited[i]){
                count++;
                visited[i] = true;
                helper(friends,visited,i);
            }
        }

        return count;
    }

    private static void helper(char[][] friends, boolean[] visited, int current){

        for(int i=0; i <visited.length; i++){
            if(!visited[i] && friends[current][i] == 'Y' && i!=current){
                visited[i] = true;
                helper(friends,visited,i);
            }
        }
    }


    public static void main(String[] args) {

        char[][] friends = {"YYNN".toCharArray(), "YYYN".toCharArray(), "NYYN".toCharArray(), "NNNY".toCharArray()};
        System.out.println(findFriendsCircle(friends));
    }
}
