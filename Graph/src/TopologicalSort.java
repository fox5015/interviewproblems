/**
 *
 * Built package Topological sort problem
 *
 */

import org.omg.PortableInterceptor.INACTIVE;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class TopologicalSort<T> {

    public Stack<Vertex<T>> topoSort(Graph<T> graph){
        Stack<Vertex<T>> stack = new Stack<>();
        Set<Vertex<T>> visited = new HashSet<>();

        for(Vertex<T> vertex:graph.getAllVertex()){
            if(visited.contains(vertex)){
                continue;
            }
            util(stack,visited,vertex);
        }

        return stack;

    }

    public void util(Stack<Vertex<T>> stack, Set<Vertex<T>> visited, Vertex<T> vertex){

        visited.add(vertex);
        for (Vertex<T> child: vertex.getAdjacentVertexes()){
            if(visited.contains(child)){
                continue;
            }
            util(stack,visited,child);
        }

        stack.push(vertex);
    }


    public static void main(String[] args) {
        Graph<Integer> graph = new Graph<>(true);
        graph.addEdge(1, 3);
        graph.addEdge(1, 2);
        graph.addEdge(3, 4);
        graph.addEdge(5, 6);
        graph.addEdge(6, 3);
        graph.addEdge(3, 8);
        graph.addEdge(8, 11);


        TopologicalSort<Integer> sort = new TopologicalSort<>();
        Stack<Vertex<Integer>> result = new Stack<>();
        result = sort.topoSort(graph);

        while (!result.isEmpty()){
            System.out.println(result.pop());
        }

    }

}
