
import java.util.HashSet;
import java.util.Set;

public class CycleInDirectedGraph {

    public boolean hasCycle(Graph<Integer> graph){

        Set<Vertex<Integer>> whiteSet = new HashSet<>();
        Set<Vertex<Integer>> greySet = new HashSet<>();
        Set<Vertex<Integer>> blackSet = new HashSet<>();

        for(Vertex<Integer> vertex: graph.getAllVertex()){
            whiteSet.add(vertex);
        }

        while(whiteSet.size() > 0){

            Vertex<Integer> current = whiteSet.iterator().next();
            if(dfs(current,whiteSet,greySet,blackSet)){
                return true;
            }
        }

        return false;
    }

    private boolean dfs(Vertex<Integer> current, Set<Vertex<Integer>> whiteSet, Set<Vertex<Integer>> greySet, Set<Vertex<Integer>> blackSet){

        moveVertex(current,whiteSet,greySet);

        for(Vertex<Integer>neighbor : current.getAdjacentVertexes()){

            if(blackSet.contains(neighbor)){
                continue;
            }

            if(greySet.contains(neighbor)){
                return true;
            }

            if(dfs(neighbor,whiteSet,greySet,blackSet)){
                return true;
            }
        }

        moveVertex(current,greySet,blackSet);
        return false;

    }

    private void moveVertex(Vertex<Integer> current, Set<Vertex<Integer>> source, Set<Vertex<Integer>> destination){

        source.remove(current);
        destination.add(current);
    }


    public static void main(String args[]){
        Graph<Integer> graph = new Graph<>(true);
        graph.addEdge(1, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 3);
        graph.addEdge(4, 1);
        graph.addEdge(4, 5);
        graph.addEdge(5, 6);
        graph.addEdge(6, 4);
        CycleInDirectedGraph cdg = new CycleInDirectedGraph();
        System.out.println(cdg.hasCycle(graph));
    }

}