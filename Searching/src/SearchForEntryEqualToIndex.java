/**
 * Search in a sorted array for i
 */
public class SearchForEntryEqualToIndex {

    public static int searchEntryEqualToIndex(int[] arr){
        int left = 0;
        int right = arr.length - 1;
        int mid = 0;
        int diff = 0;

        while (left <= right){
            mid = left + ((right - left) /2);
            diff = arr[mid] - mid;
            if (diff == 0){
                return mid;
            } else if (diff < 0){
                right = mid + 1;
            } else if (diff > 0){
                right = mid - 1;
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int result = searchEntryEqualToIndex(new int[]{-2, 0, 2, 3, 6, 7, 9});
        System.out.println(result);
    }
}
