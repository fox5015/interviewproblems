/**
 * First occurrence of k in sorted array
 */

public class FirstOccurrenceOfK {

    public static int searchFirst(int[] arr, int k){

        int left = 0;
        int right = arr.length - 1;
        int result = -1;
        int mid = 0;

        while (left <= right){
            mid = left + ((right - left)/2);
            if (arr[mid] > k){
                right = mid - 1;
            } else if (arr[mid] < k){
                left = mid + 1;
            } else if (arr[mid] == k){
                result = mid;
                right = mid - 1;
            }
        }
        return result;
    }


    public static void main(String[] args) {
        int result = searchFirst(new int[]{-14, -10, 2, 108, 108, 243, 285, 285, 285, 401}, 108);
        System.out.println(result);
    }
}
