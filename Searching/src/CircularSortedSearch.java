/**
 *  Search in Circular sorted array
 *  Inspiration from: https://www.youtube.com/watch?v=uufaK2uLnSI
 */

public class CircularSortedSearch {

    public static int circularArraySearch(int[] arr, int k){
        int left = 0;
        int right = arr.length - 1;
        int mid = 0;

        while (left <= right){
            mid = left + ((right - left) / 2);
            if (arr[mid] == k){
                return mid;
            } else if (arr[mid] <= arr[right]){ // right half is sorted
                if (k > arr[mid] && k <= arr[right]){
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            } else if (arr[left] <= arr[mid]){ //left half is sorted
                if (k >= arr[left] && k < arr[mid]){
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int result = circularArraySearch(new int[]{12, 14, 18, 21, 3, 6, 8, 9}, 3);
        System.out.println(result);
    }
}
