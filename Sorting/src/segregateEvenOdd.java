/**
 * Segregate Even and Odd numbers on left and right side respectively order does not matter
 *
 * Logic:
 * Keep on moving incrementing left pointer until we see first odd element.
 * Decrement right pointer until we see first odd element
 * then swap
 */
public class segregateEvenOdd {

    public static void partition(int arr[]){

        if(arr.length == 0){
            return;
        }

        int left = 0;
        int right = arr.length-1;

        while(left < right){

            while(arr[left] % 2 == 0){
                left++;
            }
            while(arr[right] % 2 == 1){
                right--;
            }

            if(left < right){
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
                left++;
                right--;
            }
        }
    }


    public static void main(String[] args) {

        int arr[] = {12, 34, 45, 9, 8, 90, 3};

        partition(arr);

        System.out.print("Array after segregation ");
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i]+" ");
    }

}
