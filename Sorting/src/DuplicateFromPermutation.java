/**
 */
public class DuplicateFromPermutation {

    public static int findDuplicateFromPermutation(int[] arr){

        int temp = 0;
        int inHand = 0;

        for(int i = 0; i < arr.length; i++){

            inHand = arr[i];
            arr[inHand] = Integer.MAX_VALUE;

            while(arr[inHand-1] != inHand && arr[inHand-1]!= Integer.MAX_VALUE ){

                temp = arr[inHand-1];
                arr[inHand-1] = inHand;
                inHand = temp;

                if(inHand == Integer.MAX_VALUE){
                    break;
                }
            }

            if(arr[inHand-1] == inHand){
                return inHand;
            }

        }

        return -1;

    }


    public static void main(String[] args) {

        int[] arr = {1,7,4,3,2,7,4};
        System.out.println(findDuplicateFromPermutation(arr));

    }
}
