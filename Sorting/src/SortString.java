import sun.misc.Sort;

/**
 * Input- String of Characters
 * Output- String of Characters ordered by ASCII
 */
public class SortString {

    static String sortCharacters(String input){

        StringBuilder sb = new StringBuilder(input.length());

        if(input == null || input.length() == 0){
            return sb.toString();
        }

        int[] bucket = new int[256];
        for(int i = 0; i < input.length(); i++){
            bucket[(int)input.charAt(i)]++;
        }

        int count = 0;
        for(int i = 0; i < bucket.length; i++){

            count = bucket[i];
            while(count > 0){
                sb.append((char)i);
                count--;
            }
            count = 0;
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        System.out.println(sortCharacters("camelBack"));

    }
}


