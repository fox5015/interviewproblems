/**
 * Given a set of time intervals in any order, merge all overlapping intervals into one and
 * output the result which should have only mutually exclusive intervals.
 *
 * Hint: Sort the intervals
 */

import java.util.*;
public class MergeIntervals {

    static class Interval {
        int start;
        int end;
        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "[" + start + "," + end + "]";
        }
    }


    /**
     * Function that merges the intervals
     * @param intervals List of intervals needed to be merged
     * @return List of merged intervals
     */

    public static List<Interval> merge(List<Interval> intervals){

        List<Interval> result = new ArrayList<>();

        if(intervals==null || intervals.size()==0){
            return result;
        }

        //sort the intervals depending on the start time if they are not equal, else sort on end time
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                if(o1.start!=o2.start){
                    return o1.start-o2.start;
                }
                else{
                    return o1.end-o2.end;
                }
            }
        });

        System.out.println("After Sorting:" + intervals);
        Interval pre = intervals.get(0);

        for(int i=1;i<intervals.size();i++){
            Interval current = intervals.get(i);
            if(current.start > pre.end){
                result.add(pre);
                pre = current;
            }
            else{
                Interval merged = new Interval(pre.start, Math.max(pre.end,current.end));
                pre = merged;
            }
        }

        result.add(pre);

        return result;
    }



    public static void main(String[] args) {
        ArrayList<Interval> list = new ArrayList<Interval>();
        list.add(new Interval(8, 10));
        list.add(new Interval(1, 3));
        list.add(new Interval(1, 2));
        list.add(new Interval(2, 6));
        list.add(new Interval(3, 9));
        list.add(new Interval(15, 18));
        System.out.println("Input: " + list);
        System.out.println("merged interval: " + merge(list));
    }

}
