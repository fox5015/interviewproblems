import java.util.Arrays;

/**
 * Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent,
 * with the colors in the order red, white and blue.
 */
public class SortColors {

    public static void sortColors(int[] arr){

        if(arr == null || arr.length == 0){
            return;
        }

        int[] counts = new int[3];

        for(int i:arr){
            counts[i]++;
        }
        System.out.println(Arrays.toString(counts));

        int i=0;
        for(int j=0;j < 3; j++){
            while(counts[j]-- > 0){
                arr[i++] = j;
            }
        }

    }

    public static void main(String[] args) {

        int[] arr = new int[]{0,0,1,2,2,1,2,1,0,0,2,1,0};
        sortColors(arr);

        System.out.println(Arrays.toString(arr));
    }

}
