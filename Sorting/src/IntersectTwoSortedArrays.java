import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Find intersection of two sorted arrays
 */

public class IntersectTwoSortedArrays {

    public static List<Integer> findIntersectionSortedArrays(List<Integer> A, List<Integer> B){
        int indexA = 0;
        int indexB = 0;
        List<Integer> intesection = new ArrayList<>();

        while (indexA < A.size() && indexB < B.size()){
            if (A.get(indexA) < B.get(indexB)){
                indexA++;
            } else if (A.get(indexA) > B.get(indexB)){
                indexB++;
            } else {
                intesection.add(A.get(indexA));
                indexA++;
                indexB++;
            }
        }

        return intesection;
    }


    public static void main(String[] args) {
        List<Integer> A = Arrays.asList(1,2,7,7,9);
        List<Integer> B = Arrays.asList(2,3,7,8);
        List<Integer> result = findIntersectionSortedArrays(A, B);
        System.out.println(result);
    }
}
