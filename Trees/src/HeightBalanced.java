/**
 * Check if the tree if height balanced
 */

public class HeightBalanced {


    private static int isBalancedUtil(Node root){

        if (root == null){
            return 0;
        }

        int left = isBalancedUtil(root.left);
        if (left < 0){
            return -1;
        }

        int right = isBalancedUtil(root.right);
        if (right < 0){
            return -1;
        }

        int diff = Math.abs(left-right);
        if (diff <= 1){
            return Math.max(left,right) + 1;
        } else {
            return -1;
        }

    }

    public static boolean isBalanced(Node root){
        return isBalancedUtil(root) >= 0;
    }


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node head = new Node();
        head = bt.addNode(15, head);
        head = bt.addNode(5, head);

        System.out.println(isBalanced(head));
    }
}
