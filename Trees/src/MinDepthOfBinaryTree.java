/**
 * Calculate min depth of Binary tree
 *
 *
 * Algorithm:
 1: If root is null, return 0.
 2: If root is a leaf, return 1.
 3: If left node of root is not null, recursively call getMinDepth to get leftDepth,
 else set leftDepth to MAX_VALUE
 4: If right node of root is not null, recursively call getMinDepth to get rightDepth,
 else set rightDepth to MAX_VALUE
 5: Return 1 + min of leftDepth or rightDepth

 */

public class MinDepthOfBinaryTree {

    public static int getminDepth(Node root){
        if (root == null){
            return 0;
        }
        if (root.left == null && root.right == null){
            return 1;
        }

        int leftDepth = (root.left != null) ? getminDepth(root.left) : Integer.MAX_VALUE;
        int rightDepth = (root.right != null) ? getminDepth(root.right) : Integer.MAX_VALUE;

        return 1+ Math.min(leftDepth, rightDepth);
    }
}
