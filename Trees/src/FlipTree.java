/**
 * Flip the binary tree
 */
public class FlipTree {


    private static void util(Node root){
        Node temp = root.left;
        root.left = root.right;
        root.right = temp;

        if (root.left != null){
            util(root.left);
        }

        if (root.right != null){
            util(root.right);
        }
    }

    public static void invertTree(Node root){
        if (root == null){
            return;
        } else {
            util(root);
        }
    }


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node head = null;
        head = bt.addNode(10, head);
        head = bt.addNode(15, head);
        head = bt.addNode(5, head);
        head = bt.addNode(7, head);
        head = bt.addNode(19, head);
        head = bt.addNode(20, head);
        head = bt.addNode(-1, head);
        head = bt.addNode(21, head);

        TreeTraversal.inorderIterative(head);
        invertTree(head);
        System.out.println("---");
        TreeTraversal.inorderIterative(head);
    }

}
