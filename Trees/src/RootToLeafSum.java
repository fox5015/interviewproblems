import java.util.ArrayList;
import java.util.List;

/**
 * Given a binary tree and a sum, find if there is a path from root to leaf
 * which sums to this sum.
 */

public class RootToLeafSum {

    public static boolean printPath(Node root, int sum, List<Node> path){
        if (root == null){
            return false;
        }

        if (root.left == null && root.right == null){
            if (root.data == sum){
                path.add(root);
                return true;
            } else {
                return false;
            }
        }

        if (printPath(root.left, sum - root.data, path) && printPath(root.right, sum - root.data, path)){
            path.add(root);
            return true;
        } //else {
            return false;

    }

    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node head = null;
        head = bt.addNode(10, head);
        head = bt.addNode(15, head);
        head = bt.addNode(5, head);
        head = bt.addNode(7, head);
        head = bt.addNode(19, head);
        head = bt.addNode(20, head);
        head = bt.addNode(4, head);
        head = bt.addNode(3, head);
        List<Node> result = new ArrayList<>();

        boolean b = printPath(head, 22, result);
        if (b) {
            for (int i = 0; i < result.size(); i++){
                System.out.print(head.data + "-");
            }
        } else {
            System.out.println("Did not found path for the sum");
        }
    }
}
