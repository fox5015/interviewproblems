import java.util.Stack;

/**
 * Print Iterative in order traversal
 * Refer: https://www.youtube.com/watch?v=nzmtCFNae9k&t=2s
 *
 *
 * Print Iterative pre order traversal
 * Refer: https://youtu.be/elQcrJrfObg
 *
 *
 * Print Iterative post order traversal
 * Refer: https://youtu.be/qT65HltK2uE
 */
public class TreeTraversal {

    public static void inorderIterative(Node root){

        if (root == null){
            return;
        }

        Stack<Node> stack = new Stack<>();
        while (true){
            if (root != null){
                stack.push(root);
                root = root.left;
            } else {
                if (stack.isEmpty()){
                    break;
                }
                root = stack.pop();
                System.out.println(root.data);
                root = root.right;
            }
        }
    }


    public static void preOrderIterative(Node root){
        if (root == null){
            return;
        }

        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()){
            root = stack.pop();
            System.out.println(root.data);
            if (root.right != null){
                stack.push(root.right);
            }
            if (root.left != null){
                stack.push(root.left);
            }
        }
    }

    public static void postOrderIterative(Node root){
        if (root == null){
            return;
        }

        Stack<Node> stack1 = new Stack<>();
        Stack<Node> stack2 = new Stack<>();
        stack1.push(root);
        while (!stack1.isEmpty()){
            root = stack1.pop();
            stack2.push(root);
            if (root.left != null){
                stack1.push(root.left);
            }
            if (root.right != null){
                stack1.push(root.right);
            }
        }

        while (!stack2.isEmpty()){
            root = stack2.pop();
            System.out.println(root.data);
        }
    }
}
