/**
 * Find lowestCommon ancestor in Binary tree
 * Time complexity O(n)
 * Space complexity O(h)
 * Refer: https://youtu.be/13m9ZCB8gjw
 */

public class LCABinaryTree {

    public static Node findLCA(Node root, Node n1, Node n2){
        if (root == null){
            return null;
        }

        if (root == n1 || root == n2){
            return root;
        }

        Node left = findLCA(root.left, n1, n2);
        Node right = findLCA(root.right, n1, n2);

        if (left != null && right != null){
            return root;
        }

        return (left != null) ? left : right;
    }
}
