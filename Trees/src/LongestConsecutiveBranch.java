/**
 * Binary tree: Find the length of longest consecutive branch
                 1
                  \
                  3
                 / \
                2  4
                    \
                    5
 *  in above example return 3 (1-2-3)
 *  Hint: use DFS for the solution
 */

public class LongestConsecutiveBranch {

    private static class Node{
        int value;
        Node left;
        Node right;

        Node(int data){
            value = data;
            left = null;
            right = null;
        }
    }

    private static int max(int... vals){
        int max = Integer.MIN_VALUE;
        for (int i:vals){
            if (i > max){
                max = i;
            }
        }
        return max;
    }

    private static int util(Node root, int prevVal, int length){

        //return the same length which signifies the length till that leaf
        if (root == null){
            return length;
        }
        if (root.value == prevVal +1){
            int leftVal = util(root.left, root.value, ++length);
            int rightVal = util(root.right, root.value, ++length);
            return max(leftVal,rightVal);
        } else {
            int leftVal = util(root.left, root.value, 1);
            int rightVal = util(root.right, root.value, 1);
            return max(leftVal,rightVal,length);
        }
    }

    public static int consecutive(Node root){
        if (root == null){
            return 0;
        }
        return max(util(root.left,root.value,1),util(root.right,root.value,1));
    }

    public static void main(String[] args) {

        Node root = new Node(1);
        root.right = new Node(3);
        root.right.left = new Node(2);
        root.right.right = new Node(4);
        //  root.right.right.right = new Node(5);

        System.out.println(consecutive(root));
    }





}
