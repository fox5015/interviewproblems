/**
 * Check if tree is Symmetrical
 */


public class IsSymmetrical {


    public static boolean isSymmetrical(Node tree){
        return tree == null || utilSymmetrical(tree.left, tree.right);
    }

    private static boolean utilSymmetrical(Node tree1, Node tree2){
        if (tree1 == null || tree2 == null){
            return true;
        }
        if (tree1 != null && tree2 != null){
            if (tree1.data == tree2.data){
                return utilSymmetrical(tree1.left, tree2.right) &&
                        utilSymmetrical(tree2.right, tree1.left);
            } else {
                return false;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node head = null;
        head = bt.addNode(5, head);
        head = bt.addNode(5, head);
        head = bt.addNode(5, head);

        System.out.println(isSymmetrical(head));
    }
}
