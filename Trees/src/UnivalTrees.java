/**
 * Single Value tree
 */
public class UnivalTrees {

    static int count = 0;

    private static boolean unival(Node root){
        if (root == null){
            return true;
        }
        boolean left = unival(root.left);
        boolean right = unival(root.right);

        if (left && right &&
                (root.left == null || root.left.data == root.data) &&
                (root.right == null || root.right.data == root.data)){
            count++;
            return true;
        } else {
            return false;
        }
    }
    public static int findSingleUnivalTrees(Node n){
        unival(n);
        return count;
    }

}
