/**
 * Write a program to populate right neighbor
 *
 *                          0
 *                      /     \
 *                     1       2
 *                    /     /    \
 *                  3      5      6
 *                          \    / \
 *                          9   7   8
 *
 * Refer: https://youtu.be/G46cenlnXvI
 */

public class PopulateRightNeighbors {

    private class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode neighbor;
        public TreeNode(int val){
            this.val = val;
        }
    }
    TreeNode root;

    private static void populateNeighbor(TreeNode root){
        if (root == null){
            return;
        }
        if (root.left != null){
            if (root.right != null){
                //simple case eg. 1 - 0 - 2; connect 1 and 2
                root.left.neighbor = root.right;
            } else {
                // when left is not null but right is e.g 3- 1 where we have to connect 3 to 5
                TreeNode parentNeighbor = root.neighbor;
                TreeNode neighbor;
                while (parentNeighbor != null){
                    neighbor = (parentNeighbor.left != null) ? parentNeighbor.left : parentNeighbor.right;
                    if (neighbor != null){
                        root.left.neighbor = neighbor;
                        break;
                    }
                    parentNeighbor = parentNeighbor.neighbor;
                }
            }
        }

        // eg. 9 to be connected to 7
        if (root.right != null){
            TreeNode parentNeighbor = root.neighbor;
            TreeNode neighbor;
            while (parentNeighbor != null){
                neighbor =(parentNeighbor.left != null) ? parentNeighbor.left : parentNeighbor.right;
                if (neighbor != null){
                    //Note that we are pupulating root.right.neighbor
                    root.right.neighbor = neighbor;
                    break;
                }
                parentNeighbor = parentNeighbor.neighbor;
            }
        }

         /*
          Populating neighbors in the right sub-tree before that of left sub-tree
          allows us to access all nodes at the level of current node using neighbor-node chain
          while populating neighbors for current node's child nodes.
        */

        // populate neighbors in the right sub-tree first
        populateNeighbor(root.right);

        // and then populate neighbors in the left sub-tree
        populateNeighbor(root.left);
    }

    private void traverseUsingNeighbors(TreeNode root)
    {
        TreeNode currentNode = root;
        while (currentNode != null)
        {
            TreeNode temp = currentNode;
            currentNode = null;

            // print all the nodes in the current level
            while(temp != null)
            {
                // keep checking for the left-most node in the level below the current level
                // that is where traversal for next level is going to start
                if (currentNode == null)
                {
                    currentNode = (temp.left != null) ? temp.left : temp.right;
                }

                System.out.print(temp.val + " ");
                temp = temp.neighbor;
            }
            System.out.print("\n\n");
        }
    }

    public void populateNeighbors()
    {
        populateNeighbor(root);
    }

    public void traverseUsingNeighbors()
    {
        traverseUsingNeighbors(root);
    }

    public TreeNode createTree()
    {
        this.root = new TreeNode(0);

        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n5 = new TreeNode(5);
        TreeNode n6 = new TreeNode(6);
        TreeNode n7 = new TreeNode(7);
        TreeNode n8 = new TreeNode(8);

        root.left  = n1;
        root.right = n2;

        n1.left = n3;

        n2.left = n5;
        n2.right = n6;

        n6.left = n7;
        n6.right = n8;

        return root;
    }

    public static void main(String[] args)
    {
        PopulateRightNeighbors tree = new PopulateRightNeighbors();

        /*
                                0
                          1             2
                      3              5     6
                                          7  8
        */

        tree.createTree();
        tree.populateNeighbors();
        tree.traverseUsingNeighbors();
    }
}
