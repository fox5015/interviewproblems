import java.util.Map;

/**
 * Largest BST in Binary tree
 */



public class LargestBSTInBT {

    static class MinMax{
        int max;
        int min;
        boolean isBST;
        int size;
        MinMax(){
            min = Integer.MIN_VALUE;
            max = Integer.MAX_VALUE;
            isBST = true;
            size = 0;
        }
    }
    private static MinMax findLargestBST(Node root){
        if (root == null){
            return new MinMax();
        }

        MinMax m = new MinMax();

        MinMax left = findLargestBST(root.left);
        MinMax right = findLargestBST(root.right);

        if (left.isBST == false ||  right.isBST == false || (root.data >= right.max || root.data < left.max)){
            m.isBST = false;
            m.size = Math.max(left.size, right.size);
            return m;
        }

        m.isBST = true;
        m.size = left.size + right.size + 1;
        m.max = root.right != null ? right.max : root.data;
        m.min = root.left != null ? left.min : root.data;
        return m;

    }

    public int largestBST(Node root){
        MinMax m = findLargestBST(root);
        return m.size;
    }

    public static void main(String[] args) {
        System.out.println("hi");
    }

}
