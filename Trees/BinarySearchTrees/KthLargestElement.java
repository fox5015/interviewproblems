/**
 * Find Kth largest element
 */

public class KthLargestElement {

    private static void kthLargestUtil(Node root, int k, int count){
        if (root == null) {
            return;
        }

        kthLargestUtil(root.right, k, count);
        count++;

        if (count == k){
            System.out.println(root.data);
            return;
        }

        kthLargestUtil(root.left, k, count);

    }

    public static void kthLargest(Node root, int k){
        int count = 0;
        kthLargestUtil(root, k, count);
    }


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node root = null;
        root = bt.addNode(10, root);
        root = bt.addNode(15, root);
        root = bt.addNode(-10, root);
        root = bt.addNode(17, root);
        root = bt.addNode(20, root);
        root = bt.addNode(0, root);
        IsBST isBST = new IsBST();

        kthLargest(root, 3);
    }

}
