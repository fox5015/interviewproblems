import java.util.Arrays;
import java.util.Stack;

/**
 * Implement an iterator over a binary search tree (BST). Your iterator will be initialized with the root node of a BST.
 * Calling next() will return the next smallest number in the BST. Note: next() and hasNext() should run in average O(1)
 * time and uses O(h) memory, where h is the height of the tree.
 *
 * Hint: use stack
 */


public class BSTIterator {

    private Node root;
    private Stack<Node> stack = new Stack<>();

    public BSTIterator(Node p){
        root = p;
    }

    public boolean hasNext(){
        return !stack.isEmpty() || root != null;
    }

    public int next(){
        while (root != null){
            stack.push(root);
            root = root.left;
        }
        Node current = stack.pop();
        root = root.right;

        return current.data;
    }

    public void printStack(){
        for(int i= stack.size()-1; i>=0;i--)
            System.out.println(stack.get(i).data);
    }



    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node root = null;
        root = bt.addNode(10, root);
        root = bt.addNode(15, root);
        root = bt.addNode(-10, root);
        root = bt.addNode(17, root);
        root = bt.addNode(20, root);
        root = bt.addNode(0, root);

        BSTIterator obj = new BSTIterator(root);
        obj.printStack();
        System.out.println(obj.hasNext());
        obj.next();
        obj.printStack();

    }

}
