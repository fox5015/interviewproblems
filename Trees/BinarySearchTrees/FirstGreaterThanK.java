/**
 * Find the element which is first greater than the given input
 * EPI BST 15.2
 */

public class FirstGreaterThanK {

    public static Node FindFirstGreaterThanK(Node root, int k){
        Node subtree = root;
        Node firstSoFar = null;
        while (subtree != null){
            if (subtree.data > k){
                firstSoFar = subtree;
                subtree = subtree.left;
            } else {
                subtree = subtree.right;
            }
        }

        return firstSoFar;
    }


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node root = null;
        root = bt.addNode(10, root);
        root = bt.addNode(15, root);
        root = bt.addNode(-10, root);
        root = bt.addNode(17, root);
        root = bt.addNode(20, root);
        root = bt.addNode(0, root);
        IsBST isBST = new IsBST();

        System.out.println(FindFirstGreaterThanK(root, 12).data);
    }

}
