/**
 * Check if tree is binary search tree
 */

public class IsBST {

    private static boolean isBSTUtil(Node root, int minVal, int maxVal){
        if (root == null){
            return true;
        }

        if (root.data > minVal && root.data < maxVal &&
                isBSTUtil(root.left, minVal, root.data) &&
                isBSTUtil(root.right, root.data, maxVal)){
            return true;
        }

        return false;
    }

    public static boolean isBST(Node root){
        return isBSTUtil(root, Integer.MIN_VALUE, Integer.MIN_VALUE);
    }


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node root = null;
        root = bt.addNode(10, root);
        root = bt.addNode(15, root);
        root = bt.addNode(-10, root);
        root = bt.addNode(17, root);
        root = bt.addNode(20, root);
        root = bt.addNode(0, root);
        IsBST isBST = new IsBST();

        System.out.println(isBST(root));

    }

}
