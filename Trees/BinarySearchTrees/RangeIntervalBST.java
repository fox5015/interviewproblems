import java.util.ArrayList;
import java.util.List;

/**
 * Find BST keys in the given range {k1,k2} where k1 < k2
 */

public class RangeIntervalBST {


    private static void rangeLookupBSTUtil(Node tree, int k1, int k2, List<Integer> result){

        if (tree == null){
            return;
        }

        if (tree.data >= k1 && tree.data < k2){
            rangeLookupBSTUtil(tree.left, k1, k2, result);
            result.add(tree.data);
            rangeLookupBSTUtil(tree.right, k1, k2, result);
        }
        else if (tree.data < k1){
            rangeLookupBSTUtil(tree.right, k1, k2, result);
        } else if (tree.data >= k1){
            rangeLookupBSTUtil(tree.left, k1, k2, result);
        }

    }

    public static List<Integer> rangeLookupBST(Node tree, int k1, int k2){
        List<Integer> result = new ArrayList<>();
        if (tree == null){
            return result;
        }
        rangeLookupBSTUtil(tree, k1, k2, result);
        return result;
    }


    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node root = null;
        root = bt.addNode(10, root);
        root = bt.addNode(15, root);
        root = bt.addNode(-10, root);
        root = bt.addNode(17, root);
        root = bt.addNode(20, root);
        root = bt.addNode(0, root);
        List<Integer> result = rangeLookupBST(root, 10,17);
        for (int i:result){
            System.out.print(i + "-");
        }

    }
}
