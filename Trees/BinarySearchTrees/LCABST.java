/**
 * Lowest Common ancestor in BST
 */


public class LCABST {

    public static Node findLCA(Node root, int p, int q){

        Node iter = root;
        if (iter.data > p && iter.data < q){
            return iter;
        } else if (iter.data > p && iter.data > q){
            findLCA(iter.left, p, q);
        } else if (iter.data < p && iter.data < q){
            findLCA(iter.right, p, q);
        }
        return iter;
    }

    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
        Node root = null;
        root = bt.addNode(10, root);
        root = bt.addNode(15, root);
        root = bt.addNode(-10, root);
        root = bt.addNode(17, root);
        root = bt.addNode(20, root);
        root = bt.addNode(0, root);

        System.out.println(findLCA(root, 0, 20).data);
    }

}
